# :mortar_board: Bachelorarbeit
This Repository contains all files for my Bachelor's Thesis at the Institute of Geophysics and Extraterrestrial Physics, TU Braunschweig [:link:](https://www.tu-braunschweig.de/en/igep). In this thesis ice contents in permafrost subsoils were estimated using Neural Networks and $k$-nearest Neighbor Regression.

If you find this data or the ML-models useful in your own research, please mention this data set and/or the thesis

Boinowitz, Hannah Linnéa. "Using Neural Networks for Ice Content Estimation from High-Frequency SIP Data", 2022. 

[[_TOC_]]

## Installation
1. Clone the Repository by running `git clone https://gitlab.com/h_boinowitz/bachelorarbeit.git`
2. Navigate to the folder the Repository has been cloned to
3. Run `cd code` to navigate into the directory containing the source code
4. Setup a suitable virtual envioronment by running `make install`. This command will take care of the installation of all packages in [requirements.txt](code/requirements.txt) and the [utils](code/utils)-package. 
> The name of the virtual environment defaults to `.venv`, it can be modified by exporting the environment variable `VENV_NAME` before running `make install`.

## Structure
### Notebooks
The [`notebooks`](code/notebooks) folder contains multiple Jupyter Notebooks
- The complete ice content estimation runs in [`ice_content.ipynb`](code/notebooks/ice_content.ipynb)
- Simple 1D-inversion code for Geoelectrics measurements can be found in [`1D_inversion.ipynb`](code/notebooks/1D_inversion.ipynb)

### `utils`-package
In the [`utils`](code/utils)-directory the code for the ice content estimation is stored
- SIP-specific code for creating CCM and Zorin-Ageev spectra can be found in [`sip.py`](code/utils/sip.py)
- The corresponding configuration file for the SIP-specific code is [`sip_config.py`](code/utils/sip_config.py), this file also provides formatters and LaTex-String for all quantities and units appearing in the CCM and Zorin-Ageev model
- Code for the Exploratory Data Analysis of the synthetic data set used for ice content estimation can be found in [`analysis.py`](code/utils/analysis.py)
- Code for evaluating and training ML-models is available in [`ml.py`](code/utils/ml.py)
- [`plot.py`](code/utils/plot.py) and [`etl.py`](code/utils/etl.py) provide functions for exporting the plots and tables created by the other scripts to LaTex

### Models
Trained models are stored as Pickle files in the [`models`](code/models)-directory. These models can be reimported using `sklearn`. The reimport of dumped `sklearn`-models is described in the `sklearn` Documentation at [https://scikit-learn.org/stable/modules/model_persistence.html](https://scikit-learn.org/stable/modules/model_persistence.html)

### Data
The synthetic data generated is stored as a Pickle-File in the the [`data`](code/data)-directory. The field data of Mudler et al. (2021)[^1] used in the thesis for evaluating the ML-models is stored in the `*_mudler_et_al.mat` files in the same directory. The import of the data is shown in the [`ice_content.ipynb`](code/notebooks/ice_content.ipynb)-Notebook.

[^1]: Mudler, Jan, Andreas Hördt, Dennis Kreith, Kirill Bazhin, Lyudmila Lebedeva, and Tino Radić. "Broadband Spectral Induced Polarization for the Detection of Permafrost and an Approach to Ice Content Estimation – A Case Study from Yakutia, Russia" (June 28, 2021). https://tc.copernicus.org/preprints/tc-2021-154/.

### Plots and Tables
The tables and plots used in the thesis can be found in the [`plots`](code/plots)- and [`latex_table_export`](code/latex_table_export)-directories. Paths to these directories are set in [`etl.py`](code/utils/etl.py)

