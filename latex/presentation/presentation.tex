\documentclass[fleqn,11pt,aspectratio=43]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[table,xcdraw]{xcolor}

\usepackage[style=authoryear, giveninits=true, natbib=true, uniquename=init, backend=biber]{biblatex}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{adjustbox}
\usepackage{booktabs}
\usepackage{mhchem,chemmacros}
\usepackage[labelfont=bf]{caption}
\usepackage{float}
\usepackage{soul}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{appendix}
\usepackage{cancel}
\usepackage{multicol}
\usepackage{siunitx}
\usepackage{subfig}
\usepackage{multirow}


\usepackage{physics}
\usepackage{longtable}
\usepackage{gensymb}
\usepackage{listings}
\usepackage{blindtext}
\usepackage{datetime}

\raggedbottom
\addbibresource{../sources.bib}
\renewcommand*{\nameyeardelim}{\addcomma\space}

%Codeausgabe
\lstdefinestyle{mystyle}{
  literate={{ö}{{\"o}}1
           {ä}{{\"a}}1
           {ü}{{\"u}}1
           {ß}{{\ss}}1},
    commentstyle=\color{tubsRed},
    keywordstyle=\bfseries\color{tubsOrangeMedium100},
    numberstyle=\ttfamily\tiny,
    stringstyle=\color{tubsGreenMedium100},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,    
    frame = shadowbox,
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=8pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\usepackage{setspace}

\usetheme[nexus,colorblocks,blue, dark, colorfoot, tocinheader]{tubs}
\makeatletter
\patchcmd{\beamer@sectionintoc}
  {\vfill}
  {\vskip\itemsep}
  {}
  {}
\setbeamertemplate{frametitle}[default]
\setbeamertemplate{enumerate items}[default]
\setbeamercolor*{enumerate item}{fg=tubsRed}
\setbeamercolor*{enumerate subitem}{fg=tubsRed}
\setbeamercolor*{enumerate subsubitem}{fg=tubsRed}

\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\makeatother

\author[H. Boinowitz]{Hannah Boinowitz}
\title[Ice Content Estimation with Neural Networks]{Using Neural Networks for Ice Content Estimation from High-Frequency SIP Data}
\subtitle{Bachelor's Thesis Presentation}
\newdate{date}{03}{02}{2022}
\date{\displaydate{date}}
\titlegraphic[cropped]{\tuDefaultTitlegraphic}



\begin{document}
\begin{frame}[plain]
\titlepage
\end{frame}

\begin{frame}{Agenda}
    \begin{columns}[t]
        \begin{column}{.5\textwidth}
            \tableofcontents[sections={1-4}]
        \end{column}
        \begin{column}{.5\textwidth}
            \tableofcontents[sections={5-6}]
        \end{column}
    \end{columns}
\end{frame}

\section{Motivation}
\begin{frame}{Motivation}
\begin{itemize}
	\item Existing results of ice content estimation from high-frequent spectral induced polarization (HSIP) data \citep{mudler_2021}.
	\item $1.5\si{\degree C}$ scenario: 17-44\% of the world's permafrost in danger due to climate change \citep{ipcc_2018}
	\item Quick and easy to use tools for ice content estimation are very important for scalable permafrost monitoring
\end{itemize}
\end{frame}

\section{Problem}
\begin{frame}{Problem}
\begin{itemize}
	\item Apparent resistivity data is often inverted into spectral resitivity models that do not contain ice content information
	\item Inverted data has to be converted into more suitable models using (numerical) fits
	\item Disadvantages of this strategy
    \begin{itemize}
        \item time-consuming
        \item introduces new errors to the data set 
    \end{itemize}
\end{itemize}
\end{frame}

\section{Goals}
\begin{frame}{Goals}
\begin{itemize}
	\item Deeper understanding of the models used by \citet{mudler_2021} and their mathematical properties.
	\item Proof of Concept: Example of using Neural Networks and Machine Learning for geophysical tasks on field data
	\item Faster ice content estimation $\to$ applicable for larger data sets and for real-time monitoring
\end{itemize}
\end{frame}

\section{Used Spectral Models and Classical Fit}

\subsection{Spectral Induced Polarization}
\begin{frame}{Spectral Induced Polarization (SIP)}
    \begin{itemize}
        \item geoelectrical method
        \item Spectral Induced Polarization can be used to investigate the frequency-dependency of the resistivity of the subsoil $\to$ information on the subsoil material
        \item Alternating Current with different frequencies $\omega$ is used
        \item Observation: complex-valued resistivity spectra $\sigma(\omega)$ 
    \end{itemize}
\end{frame}

\subsection{Cole-Cole Model (CCM)}
\begin{frame}{Cole-Cole Model (CCM)}
\begin{equation}\label{eq:cole_cole}
\sigma = i\omega\epsilon_0 \qty[\epsilon_\mathrm{HF} + \frac{\epsilon_\mathrm{DC} - \epsilon_\mathrm{HF}}{1+ \qty(i\omega\tau)^c}] + \frac{1}{\rho_\mathrm{DC}}.
\end{equation}
\end{frame}


\subsection{Zorin-Ageev Model}
\begin{frame}{Zorin-Ageev Model}{Two Component Model}
\begin{figure}
\centering
\includegraphics[scale=.8]{zorin_ageev_illustration.png}
\caption{The Role of Ground Ice \citep{vanHuissteden_2020}}
\end{figure}
\end{frame}
\begin{frame}{Zorin-Ageev Model}{Two Component Model}
\begin{equation}\label{eq:mixing_model}
    \tilde\sigma^k(\omega) = (1-\alpha)\,\tilde\sigma_m^k(\omega) + \alpha \tilde\sigma_i^k(\omega),     
\end{equation}
with
\begin{equation}
    \tilde\sigma_m = \sigma_m + i\omega\epsilon_0\epsilon_m,
\end{equation} 
and $\tilde\sigma_i$ is analogous to the definition of the CCM.
\end{frame}
\begin{frame}{Zorin-Ageev Model}{Two Component Model}
\begin{itemize}
	\item allows to connect spectral information with the ice content
	\item more parameters then  CCM $\to$ have to include some information on the soil
\end{itemize}
\end{frame}

\subsection{Differences and Similarities}

\begin{frame}{Differences and Similarities between the Models}
    \begin{itemize}
        \item Important to analyze as they would affect the results of the Neural Network based on CCM-data
        \item In particular: Necessary to know if the CCM is a good approximation on the Zorin-Ageev model for all ice contents $\alpha$
    \end{itemize}
\end{frame}

\begin{frame}{Differences and Similarities between the Models}
    \begin{figure}
    \includegraphics[height=.7\textheight, trim = {0 0 0 12cm}, clip]{../../code/plots/analysis/compare_ccm_two_comp_k=0.15.png}
    \caption{Comparison of CCM (orange) and Zorin-Ageev-Model (blue) for $k = 0.15$}
    \end{figure}
\end{frame}

\begin{frame}{Zorin-Ageev Spectra dependent on ice content $\alpha$}	
    \begin{figure}
    \includegraphics[height=.7\textheight, trim = {0 0 0 12cm}, clip]{../../code/plots/analysis/spectrum_comparison_alpha.png}
    \caption{Zorin-Ageev model phase spectra for different ice contents $\alpha$}
    \end{figure}
\end{frame}

\begin{frame}{Zorin-Ageev Spectra dependent on parameter $k$}	
    \begin{figure}
    \includegraphics[height=.7\textheight, trim = {0 0 0 12cm}, clip]{../../code/plots/analysis/spectrum_comparison_k.png}
    \caption{Zorin-Ageev model phase spectra for different parmeter values of $k$}
    \end{figure}
\end{frame}

\subsection{Influences on the Fitting Error}
\begin{frame}{Fitting error for different ice contents $\alpha$}	
\begin{figure}
\includegraphics[scale=.5]{../../code/plots/analysis/rmse_by_alpha_synthetic.png}
\caption{RMSE when fitting CCM on Zorin-Ageev model for different ice contents $\alpha$}
\end{figure}
\end{frame}

\begin{frame}{Fitting error for different values of $k$}	
\begin{figure}
\includegraphics[scale=.5]{../../code/plots/analysis/rmse_by_k_synthetic.png}
\caption{RMSE when fitting CCM on Zorin-Ageev model plotted over $k$}
\end{figure}
\end{frame}

\begin{frame}{Observations}
\begin{itemize}
	\item strong positive correlation of the fitting RMSE with $\alpha$ (correlation coefficient: $0.9254$)
	\item negative correlation of the fitting RMSE with $k$ (correlation coefficient: $-0.9900$)
\end{itemize}
\end{frame}

\subsection{Classical Fit}
\begin{frame}{The Numerical Fit}
\begin{figure}
\centering
\includegraphics[width=1.05\textwidth]{../../code/plots/analysis/spatial_distribution_ccm_alpha.png}
\caption{Classical numerical fit from \citet{mudler_2021}.}
\end{figure}
\end{frame}

\section{Utilization of ML-Models}
\subsection{Used ML-Models}
\begin{frame}{Used ML-Models}
    \begin{itemize}
        \item Neural Networks (Multi-Layer Preceptron)
        \item $k$-nearest Neighbor Regression
    \end{itemize}
\end{frame}

\subsection{Neural Networks}
\begin{frame}{What are Neural Networks?}
\centering
{\includegraphics[width=0.5\textwidth]{neural_network.png}\\
\textcolor{lightgray}{\footnotesize{towardsdatascience.com \\\vspace{-0.2cm}Everything you need to know about Neural Networks and Backpropagation}}}
\vspace{0.4cm}
\begin{itemize}
\item One of the most popular Machine Learning Algorithms
\item Problem: Bit of a blackbox
\item Uses Gradient Descend to reproduce the given output
\end{itemize}
\end{frame}

\begin{frame}{Why Neural Networks instead of the Classical Numerical Approach?}
    \begin{itemize}
        \item less dependent on the parameter ranges
        \item potentially faster then the numerical fit
        \item investigation of the models' properties during the feature engineering process
    \end{itemize}
\end{frame}

\subsection{$k$-nearest Neighbor Regression}
\begin{frame}{What is $k$-nearest Neighbor Regression?}
\begin{itemize}
    \item a different ML-Algorithm
    \item uses the weighted mean of the $k$ neighbors' target values to predict the target value for a given input data point $\vb{P}$
    \begin{equation}\label{eq:weighted_mean}
        \vb{y}_{\mathrm{target},\,\vb{P}} = \sum_{i \in \text{$k$ neigbors}} \norm{\vb{P} - \vb{Q_i}} 
        \sum_{i \in \text{$k$ neigbors}} \frac{1}{\norm{\vb{P} - \vb{Q_i}}} \vb{y}_{\mathrm{target},\,i}.    
    \end{equation} 
\end{itemize}
\end{frame}

\begin{frame}{Why $k$-nearest Neighbor Regression instead of Neural Networks?}
    \begin{itemize}
        \item easier to interprete, not that much of a black box
        \item faster to train, does not need multiple training epochs to converge
    \end{itemize}
\end{frame}

\subsection{Data Set}
\begin{frame}{Data Set}
\begin{itemize}
\item uniformly distributed parameters for the Zorin-Ageev-Model 
\item $600,000$ synthetic spectra (Zorin-Ageev- and Feature-based models)/ $50,000$ fitted spectra (CCM-based approach)  
\item Train-Test-Split
\begin{itemize}
\item $60 \%$ training data
\item $40 \%$ test data
\end{itemize}
$\to$ first and second data set
\item third data set: measured data 
\end{itemize}
\end{frame}

\subsection{Approaches}
\begin{frame}{Overview of the Approaches}
\begin{tabular}{c | p{0.27\textwidth} p{0.27\textwidth} p{0.27\textwidth}}
\toprule
& \textbf{First Approach} \newline (Zorin-Ageev Spectra) & \textbf{Second Approach} \newline (Cole-Cole Spectra) & \textbf{Third/Fourth Approach} \newline (Spectral Features) \\ \midrule
 \parbox[t]{2mm}{\multirow{8}{*}{\rotatebox[origin=c]{90}{Input}}} & \textcolor{tubsRed}{All values} of the Zorin-Ageev phase spectrum $\varphi(\sigma)$ in radians&  \textcolor{tubsRed}{All values}  of the CCM phase spectrum $\varphi(\sigma)$ in radians & \textcolor{tubsRed}{Extremal values}, \textcolor{tubsRed}{relation between minimum/maximum} and \textcolor{tubsRed}{mean} of the Zorin-Ageev phase spectrum $\varphi(\sigma)$ \\\bottomrule
\end{tabular}
\end{frame}

\begin{frame}{First Approach}{Learning on Zorin-Ageev-Spectra}
\begin{exampleblock}{Expected Advantages}
\begin{itemize}
	\item Works as a good benchmark as all spectral information is presented to the Neural Network\\
	$\to$ should lead to high score values 
	\item no further calculation needed after generation of synthetic spectra 
\end{itemize}
\end{exampleblock}

\begin{alertblock}{Expected Disadvantages}
\begin{itemize}
	\item very specialized $\to$ resulting model not transferable to other spectral models
	\item does not solve the initial problem 
\end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}{Second Approach}{Learning on CCM-spectra}
\begin{exampleblock}{Expected Advantages}
\begin{itemize}
	\item suits field data the model is evaluated with \citep{mudler_2021}
\end{itemize}
\end{exampleblock}

\begin{alertblock}{Expected Disadvantages}
\begin{itemize}
	\item very specialized $\to$ resulting model not transferable to other spectral models
	\item numeric fit of a CCM on the Zorin-Ageev is pricy
	\item want to get rid of the CCM in this context
	\item uses numerical fitting extensively  
\end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}{Third/Fourth Approach}{Learning on Spectral Features}
\begin{exampleblock}{Expected Advantages}
\begin{itemize}
	\item features can be designed to suit one's needs
	\item depending on the features less measured data from the field is required $\to$ time saving
\end{itemize}
\end{exampleblock}

\begin{alertblock}{Expected Disadvantages}
\begin{itemize}
	\item features have to be designed $\to$ need good knowledge about both models and their connection
	\item expected to be less precise then the approaches priorly introduced
\end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}{Third/Fourth Approach}{Learning on Spectral Features}
\textbf{Criteria for choosing Features}\vspace{.3cm}
\begin{itemize}
	\item fast to calculate
	\item strong correlation with ice content
	\item physically explainable/interpretable
\end{itemize}
\end{frame}

\begin{frame}{Third/Fourth Approach}{Learning on Spectral Features}
\begin{figure}
	\includegraphics[scale=0.4]{../../code/plots/analysis/correlation_matrix.png}
	\caption{Correlation Matrix for Features and Ice Content.}
\end{figure}

\end{frame}

\section{Results}
\subsection{Evaluation Process}
\begin{frame}{Evaluation Process}
\begin{enumerate}
	\item generate sample data and stop duration
	\item train models and stop training duration
	\item make predictions on synthetic data
	\item calculate RMSE for every sample in test and train data set
	\item calculate score (coefficient of determination) given by
	\begin{equation}
	R^2 = \qty(1 - \frac{\sum_i \qty[y_i  - y_{pred, \, i}]^2}{\sum_i \qty[y_i  - \hat{y}]^2})
	\end{equation}
	 on train and test data set 
	\item group train and test data set by input ice content $\to$ check for potential correlation between ice content $\alpha$ and prediction error
\end{enumerate}

\end{frame}

\subsection{Results on Synthetic Data}
\begin{frame}{Results on Synthetic Data}{Learning on Zorin-Ageev-Spectra}
    \input{../../code/latex_tables/_errors_zorin_ageev.tex}
\end{frame}

\begin{frame}{Results on Synthetic Data}{Learning on Zorin-Ageev-Spectra}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.4]{../../code/plots/neural_networks/alpha_pred_by_alpha_zorin_ageev_test.png}
\caption{Scatter plot of $\alpha_\mathrm{pred}$ over $\alpha_\mathrm{real}$ on test data for the first approach.}
\end{figure}
\end{column}

\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.4]{../../code/plots/neural_networks/rmse_by_alpha_zorin_ageev_test.png}
\caption{Maximum, Minimum and Mean of Predicted Values by $\alpha$ on test data for the first approach.}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Results on Synthetic Data}{Learning on CCM-spectra}
    \input{../../code/latex_tables/_errors_ccm.tex}
\end{frame}

\begin{frame}{Results on Synthetic Data}{Learning on CCM-spectra}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.4]{../../code/plots/neural_networks/alpha_pred_by_alpha_ccm_test.png}
\caption{Scatter plot of $\alpha_\mathrm{pred}$ over $\alpha_\mathrm{real}$ on test data for the second approach.}
\end{figure}
\end{column}

\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.4]{../../code/plots/neural_networks/rmse_by_alpha_ccm_test.png}
\caption{Maximum, Minimum and Mean of Predicted Values by $\alpha$ on test data for the second approach.}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Results on Synthetic Data}{Neural Network trained on Spectral Features}
    \input{../../code/latex_tables/_errors_features.tex}
\end{frame}

\begin{frame}{Results on Synthetic Data}{Neural Network trained on Spectral Features}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.4]{../../code/plots/neural_networks/alpha_pred_by_alpha_features_test.png}
\caption{Scatter plot of $\alpha_\mathrm{pred}$ over $\alpha_\mathrm{real}$ on test data for the third approach.}
\end{figure}
\end{column}

\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.4]{../../code/plots/neural_networks/rmse_by_alpha_features_test.png}
\caption{Maximum, Minimum and Mean of Predicted Values by $\alpha$ on test data for the third approach.}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Results on Synthetic Data}{$k$-nearest Neighbor Regressor trained on Spectral Features}
    \input{../../code/latex_tables/_errors_k_neigh.tex}
\end{frame}

\begin{frame}{Results on Synthetic Data}{$k$-nearest Neighbor Regressor trained on Spectral Features}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.4]{../../code/plots/neural_networks/alpha_pred_by_alpha_k_neigh_test.png}
\caption{Scatter plot of $\alpha_\mathrm{pred}$ over $\alpha_\mathrm{real}$ on test data for the fourth approach.}
\end{figure}
\end{column}

\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.4]{../../code/plots/neural_networks/rmse_by_alpha_k_neigh_test.png}
\caption{Maximum, Minimum and Mean of Predicted Values by $\alpha$ on test data for the fourth approach.}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Results on Synthetic Data}{Conclusions and Model Selection}
\begin{itemize}
\item Ratio between prediction precision and duration best for models trained on Spectral Features
\item Feature-based models provide better genralization
\end{itemize}
\end{frame}

\subsection{Results on Field Data}
\begin{frame}{Results on Field Data}{Overview}
\begin{itemize}
    \item Used field data from Yakutia \citep{mudler_2021}
    \item Evaluation on measured data set $\to$ see, if model is applicable to real world problems
\end{itemize}
\end{frame}

\begin{frame}{Results on Field Data}{Neural Network trained on Spectral Features}
\begin{figure}
\includegraphics[width=1.05\textwidth]{../../code/plots/neural_networks/neural_network_fit_features.png}
\caption{Resulting ice content section of the Neural Network trained on Spectral Features.}
\end{figure}
\end{frame}

\begin{frame}{Results on Field Data}{Neural Network trained on Spectral Features}
\begin{figure}
\includegraphics[width=1.05\textwidth]{../../code/plots/neural_networks/neural_network_fit_features_diff.png}
\caption{Difference between the ice content estimated by the Neural Network trained on Spectral Features and the classical fit.}
\end{figure}
\end{frame}

\begin{frame}{Results on Field Data}{$k$-nearest Neighbor Regressor trained on Spectral Features}
    \begin{figure}
    \includegraphics[width=1.05\textwidth]{../../code/plots/neural_networks/neural_network_fit_k_neigh.png}
    \caption{Resulting ice content section of the Neural Network trained on Spectral Features.}
    \end{figure}
\end{frame}
    
\begin{frame}{Results on Field Data}{$k$-nearest Neighbor Regressor trained on Spectral Features}
    \begin{figure}
    \includegraphics[width=1.05\textwidth]{../../code/plots/neural_networks/neural_network_fit_k_neigh_diff.png}
    \caption{Difference between the ice content estimated by the Neural Network trained on Spectral Features and the classical fit.}
    \end{figure}
\end{frame}

\section{Summary and Outlook}
\begin{frame}{Summary}
\begin{itemize}
	\item Proof of Concept: Neural Networks can be applied to this problem
	\item Even small number of features leads to satisfying results 
	\item Used models are quite different (e.g. behavior for $\omega \to \infty$) $\longrightarrow$ makes usage of Neural Networks more challenging
\end{itemize}
\end{frame}

\begin{frame}{Outlook}
\begin{itemize}
	\item Design of additional features for more precise predictions
	\item Neural Network for inversion of measured data $\to$ Just one Neural Network for all steps of the preprocessing pipeline
	\item Integrating Zorin-Ageev-Model in inversion software to be less dependent on CCM
\end{itemize}
\end{frame}

\begin{frame}{References}
\printbibliography
\end{frame}

\end{document}