\section{Bounding and Choosing Zorin-Ageev-Parameters}
In this chapter, a synthetic data set of Zorin-Ageev model parameters is generated. This data set will be used in Chapter \ref{chap:neural_nets_synthetic} to train Machine Learning models for ice content estimation from the spectra calculated based on the generated Zorin-Ageev parameters. This ML application requires a large number of samples for training the ML models. Therefore a synthetic data set is used, which is one way to obtain a sufficient number of samples.  

To generate synthetic data using the Zorin Ageev model specified in Eq. \eqref{eq:zorin_ageev} as a generative model, it is important to choose appropriate ranges for the input parameters of the generative model. This is because the training data set passed into the Neural Network determines the \emph{knowledge} of the Neural Network. Hence the training data set directly influences on which input values the trained model can make sensible predictions. This consequently impacts the model's performance on field data because the model can only produce accurate predictions on data similar to the data it has seen during its training process. 

The model's training data set should therefore include the Zorin-Ageev parameter values that are typically describing the polarization and relaxation processes of (partly) frozen subsoils so that the synthetic data set reflects the subsoil conditions. 
Therefore, it is necessary to bound the parameters of the Zorin-Ageev model accordingly before generating the data set. In particular, it is important that the data set contains all possible ice content values $\alpha \in [0,1]$ because the target values in the training data set constrain the prediction output values of the trained model. As long as the prediction input is structurally similar to the input of the samples the model sees during the training, the predicted output values should be from the same or at least a similar domain as the training target values. 

For the other parameters of the Zorin-Ageev model, typical values for the natrual constants of ice taken from \citep{bittelli_2004}, the value $k$ is bounded as described in \ref{chap:background} \citep{Zorin_2017} and the values for the matrix material are set based on the CCM model values obtained for $\rho$ and $\epsilon_\mathrm{HF}$ by \citet{mudler_2021}. The boundaries for the five free remaining Zorin-Ageev parameters are shown in Tab. \ref{tab:boundaries_synthetic_data_set}.   

Within these boundaries for the Zorin-Ageev parameters, $600,000$ uniformly distributed random sample values are created for every parameter. These values are then combined into $600,000$ complete sets of Zorin-Ageev parameters. For each of these parameter combinations the spectrum over a given frequency range $\Omega := [\omega_\mathit{min}, \omega_\mathit{max}]$ with $n$ steps can be calculated using Eq. \eqref{eq:zorin_ageev}. 

\begin{table}
\centering
\caption{Boundaries of the random ranges for the Zorin-Ageev parameters to be used for generating the synthetic data set. (\citet{bittelli_2004}, \citep{Zorin_2017} and \citep{mudler_2021})}
\begin{tabular}{ccc}
\toprule
parameter & minimal value & maximal value \\ 
\midrule
$\epsilon_m$ $\qty[1]$ & $10$ & $40$ \\ 
$k$ $\qty[1]$ & $0.1$ & $0.5$ \\ 
$\tau_i$ $\qty[\mathrm{s}]$ & $2 \cdot 10^{-5}$ & $4 \cdot 10^{-5}$ \\
$\sigma_m$ $\qty[\si{\frac{S}{m}}]$ & $2 \cdot 10^{-4}$ & $5 \cdot 10^{-4}$ \\
$\sigma_{i,\,\mathrm{DC}}$ $\qty[\si{\frac{S}{m}}]$ & $10^{-7}$ & $2 \cdot 10^{-6}$ \\ \bottomrule
\end{tabular}
\label{tab:boundaries_synthetic_data_set}
\end{table}

\section{Statistical Properties of the Data Set}
Now the statistical properties of the data set compiled in the last section shall be discussed. 
The characteristics of every parameter's distribution can be seen in Tab. \ref{tab:synthetic_data_distr}. A histogram for the parameter $\alpha$ is displayed in Fig. \ref{fig:parameters_hist}. The histograms for the other Zorin-Ageev look rather similar as per construction, all parameters of the data set are uniformly distributed. This can be verified by the histogram for the parameter $\alpha$ (see Fig. \ref{fig:parameters_hist}) that contains nearly the same number of samples in each of the $40$ equidistantly spaced bins. The data set consists of $600,000$ complete samples of Zorin-Ageev parameters sets.

\input{../../code/latex_tables/_distribution_greek.tex}

An example conductivity spectrum, given by one of the random sets of Zorin-Ageev parameters and plotted over the frequency range $\Omega = [10^2\,\si{Hz}, 10^5\,\si{Hz}]$ in 100 logarithmically equidistant steps, is shown in Fig. \ref{fig:example_spectrum_synthetic}. Two further examples can be found in the appendix (see Fig. \ref{fig:spectrum_example_1} - \ref{fig:spectrum_example_2}). Here and in the following, a split of $\sigma$ into a phase and an absolute value spectrum is chosen for the graphical representation of the complex conductivity $\sigma(\omega) \in \mathbb{C}$. 


\begin{figure}
	\centering
	\subfloat[]{
		\includegraphics[width=0.8 \textwidth]{../../code/plots/analysis/hist_synthetic_data_0.png}
	}
	\caption{Histogram of the randomly chosen and uniformly distributed Zorin-Ageev parameter $\alpha$. The histogram is plotted over $40$ equidistant bins. The average number of samples per bin for every parameter is indicated by the dashed orange line. The histograms for the other uniformly distributed Zorin-Ageev parameters look rather similar.}
	\label{fig:parameters_hist}
\end{figure}

\begin{figure}
	\includegraphics[width=0.8 \textwidth]{../../code/plots/analysis/spectrum_example_0.png}
	\caption{An example of an Zorin-Ageev spectrum $\sigma$ calculated from the parameters specified in the table shown in the plot. The spectrum $\sigma \in \mathbb{C}$ is represented by two real functions: The phase spectrum $\varphi(\omega)$ and the absolulte value spectrum $\abs{\sigma(\omega)}$.}
	\label{fig:example_spectrum_synthetic}
\end{figure}

\section{Advantages and Disadvantages of Synthetic Data}
This thesis is mainly based on synthetic data when training and evaluating the built ML models. Therefore the general properties of synthetic data should be discussed in the following section because using this kind of data in physical contexts has potential advantages and disadvantages.

On the one hand, there are some inherent problems when working with synthetic data:
One main disadvantage of synthetic data is that it will never perfectly imitate the measured signal. Even if the underlying model and its modeling code are very detailed and complex, not all of the natural effects can be taken into consideration at the same time.
This leads to the problem that Neural Networks, or any Machine Learning model trained on synthetic data, will perform significantly worse on experimentally measured field data than on this simplified data set because the measured data is more complicated structured, and noisy. Because of these differences between the synthetic and measured data, ML models trained on synthetic data sets are biased towards field data similar to the synthetically generated data. 

Another problem is the generation process of the data set itself. The used generative model (here Zorin-Ageev model) already contains assumptions about the underlying physical processes. For example, in the data set generated in the last sections, only interactions between ice and one kind of matrix material are taken into account and modeled, whereas, in real-world subsoils, commonly other components are present as well (e. g., air). An ML model trained on synthetic data based on the Zorin-Ageev model only sees data conform to this very simplified two-component schema in its training process. Hence it has a strong preference for data measured in settings, where the subsoil conditions almost correspond to this two-component model concept. This could lead to an additional bias induced by the generative model. 

On the other hand, there are also advantages when utilizing synthetic data:
Firstly, Machine Learning algorithms and methods can be applied, despite a shortage of suitable measured data. This is the case for the problem worked on in this thesis. Secondly, it is possible to create clean data without noise that would have to be filtered, to use field data for training ML models. Because synthetic data is created mathematically, it can cover cases that could not be measured experimentally due to technical limits, as the model parameters can be chosen freely. 

As mentioned above, suitable measured data has not been available for this thesis project yet.
In the German SIP-community measured data is mainly exchanged through the \emph{SIP-Archiv}\footnote{https://www.sip-archiv.de}. But the majority of available data in the \emph{SIP-Archiv} is obtained from laboratory measurements and, therefore, not suitable for training Machine Learning models for prediction on field data. In addition to that, it is not possible to store ice content data in the \emph{SIP-Archiv} as a stand-alone measured quantity yet. One other major problem encountered in the exploration of data from the SIP-Archiv that is generalizable to similar exchange platforms of other research communities is that different laboratories are working with different equipment or measuring differently spaced data points. This leads to the problem that measurements from the same data source are not formatted in the same way and not necessarily comparable. Hence they are not usable for Machine Learning purposes. Another aspect is that the size of the available data sets is often far too small for Machine Learning applications and the data sets are not always complete because not all quantities are measured in each experiment. Therefore, even fewer samples are available for a particular combination of input and target values.       