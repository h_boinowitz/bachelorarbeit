\section{Implementation of the Classical Approach}
In this section the implementation of the \emph{classical} numerical approach proposed by \citet{mudler_2021} is discussed. Discussing the numerical approach at this point is motivated by three main aspects. Firstly, the numerical results can be used as a benchmark for evaluating the outputs of the Machine Learning models introduced in the next chapter. The second aspect is that the fitting of one model on the other can be used to examine the differences and similarities between the Zorin-Ageev and the CCM. This information can be used when interpreting Machine Learning. Thirdly, the connection of the CCM and the Zorin-Ageev model will be needed for one of the Machine Learning approaches introduced in the next chapter.  

For the Machine Learning task at hand, the ice content is the target value. Therefore the synthetic data set contains sets of Zorin-Ageev Model parameters. Due to this, it is not possible to do the fit a Zorin-Ageev model to a given CCM as suggested by \citet{mudler_2021}. Hence the fit is done in the other direction. A CCM is fitted to each Zorin-Ageev model.

The absolute value and phase spectra are calculated for a frequency range $\Omega = \qty[10^2\,\si{Hz}, 10^5\,\si{Hz}]$ in 100 logarithmically equidistant steps, starting from the synthetic data set described in the last chapter. The phase and absolute value spectra are combined into one array (see Listing \ref{lst:zorin-ageev}, line \ref{lst:stacked}). As the phase and absolute value spectra are both real-valued, the fit can be performed on an array of real values. This is necessary as the \texttt{curve\_fit} method used for fitting the CCM spectrum to each Zorin-Ageev model spectrum can not deal with values $y \in \mathbb{C}$. 

For the curve-fitting procedure, a least-square fit with the Levenberg-Marquardt algorithm is used \citep{more_1978}. An array of start values \texttt{p\_0} is passed to the fitting method. This array contains typical values for the permittivities, the relaxation time $\tau_i$ of the Zorin-Ageev model, a constant start value for $c$ of $0.7$ as well the inverse of the DC-conductivity of ice $sigma_{i,\, DC}$ as start value for the DC-resistivity of the Zorin-Ageev model.

As the \texttt{curve\_fit} is called without the \texttt{method} argument (see Listing \label{lst:zorin-ageev}, Line \label{lst:linear_loss}), the \texttt{method} parameter defaults to Levenberg-Marquardt (\texttt{'lm'}) for unbounded curve fitting problems\footnote{Scipy-Documentation for the \texttt{curve\_fit} method is available at \url{https://docs.scipy.org/doc/scipy/reference/reference/generated/scipy.optimize.curve_fit.html#scipy.optimize.curve_fit}, last request January 25, 2022.}. The maximum number of iterations (\texttt{maxfev}) is set to $5,000$. The fitting procedure in Listing \ref{lst:fitting-classic}) is applied to $50,000$ samples. This smaller number of samples is chosen as the \texttt{curve\_fit} method is not optimzed for application on large data sets. Hence the fitting procedure is time-consuming and not applicable to the entire data set without significantly extending the running time. Using the described settings the least square fit converges for $45,710$ samples. This leads to a dropout of $4,290$ or $8.58\%$ samples. 
An example of a successful fit can be seen in Fig. \ref{fig:successful_fit}.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../../code/plots/analysis/spectrum_example_0_zorin_ageev_ccm.png}
    \caption{Example Zorin-Ageev spectrum (blue) and a classical numerically fitted Cole-Cole spectrum (orange). Plotted over the frequency range $\Omega = \qty[10^2\,\si{Hz}, 10^5\,\si{Hz}]$ in 100 logarithmically equidistant steps.}
    \label{fig:successful_fit}
\end{figure}

\begin{listing}
    \begin{minted}[linenos,tabsize=2,breaklines, obeytabs, escapeinside=!!]{python}
    def zorin_ageev_spectrum_from_parameters(row: List[int], complex: bool = True) -> np.array:
        alpha, epsilon_m, sigma_m, k, tau_i, sigma_i_DC = row
        sigma_m_tilde = sigma_m + 1j * omega * EPSILON_0 * epsilon_m
        sigma_i_tilde = sigma_i_DC + 1j * omega * EPSILON_0 * (epsilon_i_hf + (epsilon_i_DC - epsilon_i_hf)/(1 + (1j * omega * tau_i) ** c))
        sigma = ((1 - alpha) * sigma_m_tilde ** k + alpha * sigma_i_tilde ** k) ** (1/k)
        if complex:
            return sigma
        else:
            return np.hstack([np.abs(sigma), np.angle(sigma)]) !\label{lst:stacked}!
    \end{minted}
    \caption{Code for calculating the Zorin-Ageev spectrum $\sigma$ from a given array of Zorin-Ageev model parameters using Eq. \eqref{eq:zorin_ageev}}
    \label{lst:zorin-ageev}
\end{listing}

\begin{listing}
    \begin{minted}[linenos,tabsize=2,breaklines, obeytabs, escapeinside=!!]{python}
    i = 0

    def fit_from_zorin_ageev_to_ccm(row: pd.DataFrame, get_params: Optional[bool] = False):
        global i
        i += 1
        spectrum_zorin_ageev = np.array(row['spectrum_zorin_ageev'])
        try:
            popt, _ = curve_fit(spectrum_ccm_fitting, np.hstack([omega, omega]), spectrum_zorin_ageev, p0 = [5, 95, row['tau_i'], 0.7, 1/row['sigma_i_DC']], maxfev=5000)!\label{lst:linear_loss}!
        except Exception as err:
            return np.nan
        spectrum_ccm = spectrum_ccm_fitting(omega, *popt)
        if i % 1000 == 0:
            print(f'{i} fits finished.')
        if get_params:
            return spectrum_ccm, *popt
        return spectrum_ccm
    \end{minted}
    \caption{Code for finding the best-fitting CCM spectrum for a given Zorin-Ageev model.} 
    \label{lst:fitting-classic}
\end{listing}

\section{Differences and Similarities between the spectral models}
In this section, the numerical fitting results from the previous section shall be discussed. In the fitting procedure, the information from one spectral is converted to the second one as best as possible. Hence the fitting error shows how much of the Zorin-Ageev model's information is contained in the CCM. Therefore the fitting error is used to investigate structural differences and similarities between the two spectral models used in this thesis.
The intention behind this investigation is to understand the connection between both models. This knowledge is needed for the implementation of the Neural Networks that will be used for ice content estimation based on CCM spectral information.

One parameter that is only present in the Zorin-Ageev Model is $k$, the exponent in Eq. \eqref{eq:zorin_ageev}. It contains information about the material's structure on the pore-scale \citep{Zorin_2017}. It is rather complicated to sensibly bound this parameter when starting the fitting process from the Zorin-Ageev Model as $k$ is a model-specific parameter that can not be directly interpreted physically. Additionally, literature values are much rarer compared to the other better-known parameters of the Zorin-Ageev model that are mostly natural constants describing the polarization of ice and the matrix material. Here the same bounds as chosen for $k$ in the generation process of the synthetical data set are used, meaning $k \in [0.1, 0.5]$. 

It is observed that for small values of $k$, the spectra of the CCM and the Zorin-Ageev model are not fitting that well. An example for $k = 0.15$ is shown in Fig. \ref{fig:example_k_single}. To visualize how models differ depending on the value of $k$, spectra are plotted for ten equidistant values of $k$ and fixed remaining parameters (see Fig. \ref{fig:compare_spectrum_k}). Fig. \ref{fig:compare_spectrum_k} illustrates, the parameter $k$ has a strong impact on the model's spectral structure. It is investigated how strongly this parameter is correlated with the fitting error. The strength of this correlation is used to estimate how much the parameter $k$ changes the information contained in the Zorin-Ageev spectrum.

\begin{figure}
	\includegraphics[width=0.8 \textwidth]{../../code/plots/analysis/spectrum_comparison_k.png}
	\caption{Zorin-Ageev spectra for different values of $k$. The value of $k$ for every spectrum is color coded. The remaining parameters Zorin-Ageev remain constant. With $\alpha = 0.4$, $\epsilon_m = 10$, $\sigma_m = 3 \cdot 10^{-4} \,\si{\frac{S}{m}}$, $\tau_i = 3 \cdot 10^{-5} \,\si{s}$, $\sigma_{i,\,\mathrm{DC}} = 1 \cdot 10^{-6} \,\si{\frac{S}{m}}$.}
	\label{fig:compare_spectrum_k}
\end{figure}

\begin{figure}
    \includegraphics[width=.8\textwidth]{../../code/plots/analysis/compare_ccm_two_comp_k=0.15.png}
    \caption{Comparison of a Zorin-Ageev (blue) and a CCM spectrum (orange). The Zorin-Ageev parameter $k$ is set to $k = 0.15$. The remaining Zorin-Ageev model parameters are set to the same values as in Fig. \ref{fig:compare_spectrum_k}. The CCM is then numerically fitted to the Zorin-Ageev spectrum given by the specified parameter combination using a least-square fit.}
    \label{fig:example_k_single}
\end{figure}


Another parameter that is examined further is the ice content $\alpha$ itself. It is evaluated whether the CCM is a good approximation on the Zorin-Ageev model for all ice contents $\alpha \in \qty[0, 1]$. This is crucial to know when using the ice content as an output variable of the ML models and learning on CCM spectra. If the CCM was not a good approximation on the Zorin-Ageev model, that means the ice content information is not contained in the CCM in the same way as in the Zorin-Ageev model for all possible ice contents. It should be hard for a Neural Network to retrieve information on the ice contents for ice contents showing particularly large fitting errors between the two spectral models. A large fitting error would indicate that both models produce quite different spectra. As the fitting error is a metric for how differently the models represent the information about the specific polarization behavior of ice. This would lead to a different effect of ice on the frequency-dependent conductivity. 

To examine how the fitting error is correlated with both parameters $k$ and $\alpha$, the fitted spectra from the last section are used. The fitting error is calculated for each of the phase spectra $\varphi(\omega)$. To measure this error the \emph{Rooted Mean Squared Error} (RMSE) defined by
\begin{equation}
\mathrm{RMSE} = \sqrt{\frac{1}{N}\sum_{i=1}^N \qty(\hat{y}_i - \xi_i)^2} = \frac{1}{\sqrt{N}} \norm{\hat{\vb{y}} - \boldsymbol{\xi}}_2 
\end{equation}
is used, with $N$ being the number of samples, $\hat{\vb{y}}$ denoting a vector containing the expected target values and  $\boldsymbol{\xi}$ being the modelled output vector. The $\mathrm{RMSE}$ between Zorin-Ageev model and the corresponding CCM is calculated for each sample. A histogram illustrating the distribution of the RMSE can be seen in Fig. \ref{fig:dist_rmse}. The distribution shows that in general the fitting error is quite small ($< 0.01$) larger fitting errors are rare and occur only in some extreme edge cases. 

\begin{figure}
    \includegraphics{../../code/plots/analysis/dist_rmse.png}
    \caption{Histogram displaying the distribution of the fitting error (RMSE) of the numerical fitting procedure. The fitting procedure is applied to $50,000$ randomly chosen samples of Zorin-Ageev parameter sets from the synthetic data set. The histogram used equidistantly spaced bins.}
    \label{fig:dist_rmse}
\end{figure}

Both parameters $k$ and $\alpha$ are rounded to two digits to smooth the data and to reduce statistical fluctuations by taking multiple samples into account. Using this data, the Pearson correlation coefficient $R$\footnote{This is the default correlation metric of the \texttt{pandas} Python-Library used for the handling of structured data in this thesis.} between the RMSE and $k$ or $\alpha$ respectively, which is given by
\begin{equation}\label{eq:correlation_coeff}
R(\hat{\vb{y}}, \boldsymbol{\xi}) = \frac{\sum_i (\boldsymbol{\xi}_i - \expval{\boldsymbol{\xi}})(\hat{\vb{y}}_i - \expval{\hat{\vb{y}}})}{\sqrt{\sum_i (\boldsymbol{\xi}_i - \expval{\boldsymbol{\xi}})^2} \sqrt{\sum_i (\hat{\vb{y}}_i - \expval{\hat{\vb{y}}})^2}}
\end{equation}
is calculated, with the mean $\expval{\vb{a}}$ of a vector $\vb{a}$ being defined as $\expval{\vb{a}} = \frac{1}{N} \sum_i \vb{a}_i$.
The resulting correlation coefficients are $0.9254$ for the ice content and $-0.9900$ for the parameter $k$. This large positive correlation coefficient for $\alpha$ indicates increasing fitting errors between the Zorin-Ageev model and the CCM for increasing values of $\alpha$. In contrast, larger parameter values for $k$ condition smaller $\mathrm{RMSE}$s as $k$ is negatively correlated with the fitting error. Hence the spectra of both models become more "similar" for large values of $k$ and small values of $\alpha$. 

To further investigate the dependencies between the fitting errors and the parameters $\alpha$ and $k$, the data is grouped by $\alpha$ and the average is calculated in each group. The results of this calculation are used to plot the average RMSE in each ice content interval $\qty[\alpha_i, \alpha_{i+1}] \quad \forall i \in [0, 100]$ with $\alpha_i = 0.01 i$ (see Fig. \ref{fig:rmse_by_alpha}). The same algorithm is used to calculate the mean fitting error for each interval of parameter values $\qty[k_i, k_{i+1}]$. A plot of the average RMSE over the values of $k$ can be seen in Fig. \ref{fig:rmse_by_k}.

\begin{figure}
    \includegraphics{../../code/plots/analysis/rmse_by_alpha_synthetic.png}
    \caption{Plot of the mean RMSE between the given Zorin-Ageev model and the fitted CCM over the ice content $\alpha$ of the corresponding Zorin-Ageev model. In each ice content intervall $\qty[\alpha_i, \alpha_{i+1}]$ the average fitting error of several samples is calculated, so that multiple samples are taken into account. $k$ is uniformly randomly distributed (see Chap. \ref{chap:synthetic_data_set}).}
    \label{fig:rmse_by_alpha}
\end{figure}
\begin{figure}
    \includegraphics{../../code/plots/analysis/rmse_by_k_synthetic.png}
    \caption{Plot of the mean RMSE between the given Zorin-Ageev model and the fitted CCM over the Zorin-Ageev model's parameter $k$. In each intervall $\qty[k_i, k_{i+1}]$ the average fitting error of several samples is calculated, so that multiple samples are taken into account. $\alpha$ is uniformly randomly distributed (see Chap. \ref{chap:synthetic_data_set}).}
    \label{fig:rmse_by_k}
\end{figure}

It can be observed that the parameter $k$ is negatively correlated with the fitting error ($R < 0$). The ice content $\alpha$, in contrast, is positively correlated ($R > 0$) for ice contents $\alpha \lesssim 0.95$ and shows declining fitting errors on both boundaries of the range of possible ice contents. Hence the fitting error decreases for $\alpha \to 0$ and  $\alpha \to 1$. Both observations are also proved mathematically (see Appendix \ref{chap:proofs_for_findings}).

\section{Discussion}
The results from the last section show that both parameters $k$ and $\alpha$ are strongly correlated with the fitting error and that CCM and Zorin-Ageev model are only equivalent for some very specific edge cases ($k=1$ or $\alpha \in \{0, 1\}$). In all other cases, the numerical fitting procedure described above provides only the best fits, and fitting errors occur. These fitting errors will be automatically transferred into all Machine Learning models working on CCM data. This consequently impacts the prediction accuracy of CCM-based models.

The identified extreme limiting cases for the ice content $\alpha$ show that the ice content information of the Zorin-Ageev model can not be represented in the CCM. In general, the CCM conductivity phase spectrum $\varphi(\omega)$ can not fully represent the additional signal caused by the polarization effects of the ice component in the subsoil. Mathematically this is caused by the larger number of free parameters in the Zorin-Ageev model compared to the CCM.