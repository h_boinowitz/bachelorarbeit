\section{Data Set}
In this chapter, the models trained in the previous chapter are be applied to field data. This is to examine whether the models are transferable from synthetic to real-world data.  
The data set of \citet{mudler_2021} is used to evaluate the models. The resistivity data was measured in Yakutia, Russia, in May 2019 using the Cameleon II for high-frequency SIP measurements \citep{mudler_2021}. The results of these measurements are presented as a two-dimensional array of CCM parameter sets that can be used for calculating resistivity or conductivity spectra over a given frequency domain $\Omega$. The measurements cover a two dimensional profile of $45 \,\si{m}$ length and $8 \,\si{m}$ depth. In total, the data set consists of $780$ sets of Cole-Cole parameters.

The statistical distribution of each Cole-Cole parameters is illustrated in Fig. \ref{fig:parameters_hist_ccm}. Additional statistical properties of the distribution of the parameters are shown in Tab. \ref{tab:field_data_distr}. The two dimensional profile of every CCM parameter is plotted to illustrate how the Cole-Cole parameters differ spatially (see Fig. \ref{fig:spatial_distr_parameters_field_data}, after \citet{mudler_2021}).

\input{../../code/latex_tables/_distribution_ccm_greek.tex}

\begin{figure}
    \subfloat[]{
        \includegraphics[width=0.49 \textwidth]{../../code/plots/analysis/hist_field_data_0.png}
    }
    \subfloat[]{
        \includegraphics[width=0.49 \textwidth]{../../code/plots/analysis/hist_field_data_1.png}
    } \\

    \subfloat[]{
        \includegraphics[width=0.49 \textwidth]{../../code/plots/analysis/hist_field_data_2.png}
    }
    \subfloat[]{
        \includegraphics[width=0.49 \textwidth]{../../code/plots/analysis/hist_field_data_3.png}
    } \\
    
    \subfloat[]{
        \includegraphics[width=0.49 \textwidth]{../../code/plots/analysis/hist_field_data_4.png}
    }
    \caption{Histograms of the CCM parameter values in the field data set for each parameter. The histogram for every parameter is plotted over $40$ equidistant bins. The average number of samples per bin for every parameter is indicated by the dashed orange line.}
    \label{fig:parameters_hist_ccm}
\end{figure}

\begin{figure}
    \centering
    \vspace{-3cm}
    \subfloat[]{
        \includegraphics[width=0.9 \textwidth]{../../code/plots/analysis/spatial_distribution_ccm_rho.png}
    } \\
    \subfloat[]{
        \includegraphics[width=0.9 \textwidth]{../../code/plots/analysis/spatial_distribution_ccm_epsilon_DC.png}
    } \\
    \subfloat[]{
        \includegraphics[width=0.9 \textwidth]{../../code/plots/analysis/spatial_distribution_ccm_epsilon_HF.png}
    } \\
    \subfloat[]{
        \includegraphics[width=0.9 \textwidth]{../../code/plots/analysis/spatial_distribution_ccm_tau.png}
    } \\
    \subfloat[]{
        \includegraphics[width=0.9 \textwidth]{../../code/plots/analysis/spatial_distribution_ccm_c.png}
    } 
    \caption{Spatial distribution of the CCM parameter values in the field data set for each parameter in the subsoil on the test site. Plots after \citet{mudler_2021}.}
    \label{fig:spatial_distr_parameters_field_data}
\end{figure}

The ice content values \citet{mudler_2021} obtained by employing a classical numerical fit are used as the target value for evaluating the previously trained ML models in a predictive use case. There are no other reference values available for this test site other than borehole measurements at $x=40\,\si{m}$ used by \citet{mudler_2021} for the evaluation of their numerical results. The spatial distribution of the numerically estimated ice content can be seen in Fig. \ref{fig:spatial_distr_ice_content}.

\begin{figure}
    \centering
    \includegraphics[width=0.9 \textwidth]{../../code/plots/analysis/spatial_distribution_ccm_alpha.png}
    \caption{Spatial distribution of the numerically estimated ice content for the subsoil on the test site. Plot after \citet{mudler_2021}.}
    \label{fig:spatial_distr_ice_content}
\end{figure}

\section{Application of Machine Learning Models}
The input data is prepared for applying the trained Neural Networks to the measured data. Therefore the corresponding spectrum for every set of CCM parameters is calculated over a frequency range $\Omega\in [10^2\,\si{Hz}, 10^5\,\si{Hz}]$ using Eq. \eqref{eq:ccm}. For the evaluation of the third and fourth model, the spectral features specified in the previous chapter are calculated for each of the resulting Cole-Cole spectra\footnote{Here the CCM is used for the calculation of the spectral features, in contrast to the previous chapter where the spectral features have been calculated starting from a Zorin-Ageev model}. The target values have to be scaled in the same way as for the models' test and train data set to ensure the error metrics are comparable to those calculated for the train and test data set. Then each of the trained models can be applied to the prepared field data. For every model, the score and RMSE are calculated based on the scaled target values.

All of the scores and RMSEs on the field data set are shown in Tab. \ref{tab:errors_zorin_ageev} - \ref{tab:errors_k_neigh} along with the respective values on the training and test data set, where the models have been applied to synthetic data. As for the train and test data sets, the dependecy between the RMSE and the ice content $\alpha$ is investigated to see if the prediction error is correlated with the target values. Therfore the fitting error is plotted over the ice content value for all four models (see Fig. \ref{fig:rmse_by_alpha_zorin_ageev_measured} - \ref{fig:rmse_by_alpha_k_neigh_measured}). The two-dimensional ice content profiles predicted by every ML model (see Fig. \ref{fig:neural_network_fit_zorin_ageev} - \ref{fig:neural_network_fit_k_neigh}) as well as the difference picture between the ML prediction and the numerical estimation (see Fig. \ref{fig:neural_network_fit_zorin_ageev_diff} - \ref{fig:neural_network_fit_k_neigh_diff}) are plotted.

\input{../../code/latex_tables/_errors_zorin_ageev.tex}
\input{../../code/latex_tables/_errors_ccm.tex}
\input{../../code/latex_tables/_errors_features.tex}
\input{../../code/latex_tables/_errors_k_neigh.tex}

\begin{figure}
    \centering
    \includegraphics[width=0.9 \textwidth]{../../code/plots/neural_networks/neural_network_fit_zorin_ageev.png}
    \caption{Spatial ice content distribution predicted by the Neural Network trained on the Zorin-Ageev phase spectrum $\varphi(\omega)$.}
    \label{fig:neural_network_fit_zorin_ageev}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.9 \textwidth]{../../code/plots/neural_networks/neural_network_fit_ccm.png}
    \caption{Spatial ice content distribution predicted by the Neural Network trained on the CCM phase spectrum $\varphi(\omega)$.}
    \label{fig:neural_network_fit_ccm}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.9 \textwidth]{../../code/plots/neural_networks/neural_network_fit_features.png}
    \caption{Spatial ice content distribution predicted by the Neural Network trained on four spectral features representing properties of the Zorin-Ageev phase spectrum $\varphi(\omega)$.}
    \label{fig:neural_network_fit_features}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.9 \textwidth]{../../code/plots/neural_networks/neural_network_fit_k_neigh.png}
    \caption{Spatial ice content distribution predicted the $k$-nearest Neighbor Regressor trained on four spectral features representing properties of the Zorin-Ageev phase spectrum $\varphi(\omega)$.}
    \label{fig:neural_network_fit_k_neigh}
\end{figure}

\section{Discussion}
As can be seen from the ice content profile plots, the Neural Networks qualitatively produced results quite similar to the numerical estimated ice content distribution in the subsoil. Especially the typical structure of permafrost soils (see Fig. \ref{fig:structure_permafrost_soil}) is well reproduced by the Neural Network. The models all predicted an active thawed layer that is approximately $1 \,\si{m}$ thick. This active layer is followed by a frozen layer with a thickness of about $1\,\si{m}$ and reaches into depths of approximately $2 \,\si{m}$, where higher ice contents are encountered. Additionally a prominent unfrozen talik structure (in depths between $2 \,\si{m}$ and $6 \,\si{m}$), is principally observable in all models' predictions, but it is differently pronounced.      

Quantitatively the prediction of the ML models differs significantly from the numerically obtained ice content estimates and between models. The models lead to very differently precise predictive results. 

The spectral models, which use complete Zorin-Ageev spectra or Cole-Cole spectra as inputs, learned a very specific spectral structure in their training process. They are less transferable to differently structured spectral data from the field. These approaches seem to be too task-specific and not well generalizable. This problem becomes even more visible as the Zorin-Ageev model is applied to the field data using the CCM spectra that were directly inverted from field data. Consequently, the model input does not follow the specific spectral structure of the Zorin-Ageev phase spectrum. The Zorin Ageev-model has the largest prediction error on the field data (RMSE of $19.435$), whereas this model was the most accurate on the training and test data (RMSEs of about $1.68$). This shows that the model trained directly on Zorin-Ageev spectra is not able to transfer from one spectral type to the other as it has an overly detailed knowledge of the specific connection between the ice content and the corresponding Zorin-Ageev model. It is assumed that a model trained on combined training and test set containing CCM as well as Zorin-Ageev spectra would be able to predict the ice content regardless of which of the two spectral types are used as input spectra. 

The third Neural Network, which is trained on more general information provided as scalar features, has higher predictive accuracy on the field data than the Zorin-Ageev model. The model trained on spectral features produces fitting errors comparable to the fitting errors of the model trained on CCM data. This approach seems to be better transferable than the approach based on the Zorin-Ageev model. It is to be considered that the model trained on complete Zorin-Ageev spectra is applied to CCM spectra, and the model based on CCM spectra is trained on a much smaller data set. Hence the results obtained do not necessarily prove the model trained on spectral features to be better generalizable in general. Nevertheless, the results show that the much more condensed input information in the form of scalar spectral features is sufficient to achieve comparable results in predictive use cases.  

The $k$-nearest Neighbor Regressor trained on spectral features produces the best results on field data qualitatively with an RMSE of $10.52$. This is most likely caused by the effect that this simplistic model finds more general trends in the training data set. Hence this model is able to predict ice contents on scalar features calculated based on a CCM instead of a Zorin-Ageev model for the field data set. Presumably, the generalization capabilities model would also further improve when using a combined training and test set of Zorin-Ageev and CCM spectra. 


\begin{figure}
    \centering
    \includegraphics[width=0.9 \textwidth]{../presentation/zorin_ageev_illustration.png}
    \caption{The Role of Ground Ice \citep{vanHuissteden_2020}}
    \label{fig:structure_permafrost_soil}
\end{figure}
