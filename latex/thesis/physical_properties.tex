% !TEX root = main.tex
\section{Induced Polarization}
This section is mainly based on \citet{dgg_2019} and \citet{Berktold_2005}.

\subsection{Measuring Methods}
Induced Polarization (IP) is a Geoelectric method to examine the frequency-dependence of the resistivity of a geomaterial. In a Geoelectricial four-point measurement, two electrodes are used to inject the current $I$ into the subsoil, and two other electrodes measure the resulting voltage $U$ (see Fig. \ref{fig:four_point_measurement}\,(a) from \citet{Bücker_2017}). Different electrode configurations are used for different measurements. The choice of the configuration depends on the depth and width of the profile to be measured.
Three of the most commonly used electrode configurations can be seen in Fig. \ref{fig:four_point_measurement}\,(b).

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../presentation/four_point_measurement.png}
    \caption{(a) Schematic representation of a four-point Geoelectric mesurement. The two input electrodes on the left and right are used to inject a current $I$ into the ground, for IP-measurements Alternating Current (ACs) is used. The other two electrodes in the middle measure the resulting voltage $U$ (marked $V$ in the figure). (b) Three commonly used electrode configurations. Figure out of \citet{Bücker_2017}.}
    \label{fig:four_point_measurement}
\end{figure}

Each of these configurations has a specific geometry factor $K$ given by
\begin{equation}\label{eq:geometry_factor}
    K = 2\pi\qty(\qty(\frac{1}{r_1} - \frac{1}{r_2}) - \qty(\frac{1}{r_3} - \frac{1}{r_4}))^{-1}
\end{equation}
where $r_i$ denotes the distance between the $i$-th and $i + 1$-th electrode of the four-point configuration. The electrodes are enumerated from left to right. 

IP-effects in the field or in laboratory experiments are measured using two different approaches. One approach is the time-domain IP, where a series of direct currents with alternating polarities are brought into the ground in equidistant time intervals $\Delta t$. If the direct current is turned off abruptly and the time intervals $\Delta t$ are small enough, IP-effects can be observed. These IP-effects lead to a transient electromagnetic response of the subsoil. Time-frequent IP investigates the transient electromagnetic response of the subsoil by measuring the decay of a Direct Current (DC) after it is turned off.      
 
The second technique is frequency-domain Induced Polarization, where alternating current with a certain frequency $\omega$ is brought into the ground through the two input electrodes. In frequency-domain measurements, the impedance $Z(\omega)$ of the subsoil is measured with an impedance-spectrometer at multiple frequencies $\omega$. This is to investigate the frequency-dependence of the impedance. The impedance is given by
\begin{equation}
Z = \frac{U(\omega)}{I(\omega)}
\end{equation}
where both the alternating current $I$ and the voltage $U$ are complex-valued.
The absolute value $|Z| = \sqrt{\mathrm{Im}^2\,{Z} + \mathrm{Re}^2\,{Z}}$ and the phase $\varphi = \arctan \frac{\mathrm{Im}\,{Z}}{\mathrm{Re}\,{Z}}$ of the complex-valued impedance $Z$ are used to calculate the complex resistivity $\rho$. $\rho$ can be derived from the impedance by multiplying it with the configuration factor $K$ of the four-electrode array (Eq. \eqref{eq:geometry_factor}). Hence $\rho$ is given by
\begin{equation}
    \rho = K\, \frac{U}{I}.
\end{equation}
In this thesis, the conductivity $\sigma$, given by $\sigma = \rho^{-1}$, is often used instead of the resistivity itself.

The resistivity frequency effect (RFE) is defined by
\begin{equation}
    \mathrm{RFE} = \frac{\abs{Z(\omega_1)} - \abs{Z(\omega_2)}}{Z(\omega_2)}
\end{equation}
The frequency effect can be written in terms of conductivities
\begin{equation}
    \mathrm{FE} = \frac{\abs{\frac{1}{\sigma(\omega_1)}} - \abs{\frac{1}{\sigma(\omega_2)}}}{\abs{\frac{1}{\sigma(\omega_1)}}}
\end{equation}
this can be simplified to 
\begin{equation}\label{eq:frequency_effect}
    \mathrm{FE} = \frac{\abs{\sigma(\omega_2)}}{\abs{\sigma(\omega_1)}} - 1
\end{equation}

\subsection{IP-Effects in Permafrost Soils}
Using IP-methods, it is observed the conductivity of frozen water is complex-valued and frequency-dependent. The polarization behavior of ice has been theoretically examined in laboratory experiments by \citet{bittelli_2004}.
As water ice is contained in permafrost soils, the IP-effects of water ice are observed in permafrost soils.

Changing temperatures lead to a change in the lattice structure of ice. This is because the changing thermal energy deforms the lattice. Liquid water has a strong permanent dipole moment. The lattice deformation leads to a reorientation of these dipole moments and a resulting change in the charge distribution as well as in the polarization of the lattice. Hence the polarization of ice and the charge defects are temperature-dependent (\citep{Grimm_2015}, \citep{Maierhofer_2021}). 

The charge defects have a polarization behavior that can be described using a Debye model \citep{Grimm_2015}. This polarization behavior is different from the polarization behavior of the matrix material in the permafrost soil.
The charge defects are dependent on the temperature. Consequently, the temperature impacts the relaxation behavior of ice. Higher temperatures lead to a higher relaxation frequency of ice. Hence a strong increase in conductivity at high frequencies ($\omega \gg 1\,\si{kHz}$) can be observed \citep{Grimm_2015}.

\subsection{Causes of Induced Polarization (IP) Effects}
Induced polarization effects are mainly caused by two material properties.

The first cause to be considered is the formation of electric double layers (EDL) on the rock surface.
This is due to electrochemical interactions between the negative surface charge of the matrix material and the anions and cations of the pore space electrolyte. These interactions lead to a conductivity distribution, with a stable negative layer on the mineral surface with a surface charge $\sigma_\mathit{surf}$ and an adjacent positive layer outside the mineral in the pore fluid.

The positive part of the electric double layer (EDL) consists of a thin stable layer of bounded cations, the so-called Stern-Layer. As this layer does not completely compensate the negative surface charge $\sigma_\mathit{surf}$, a thicker diffuse layer of cations and anions is formed adjacent to the Stern-Layer. The number of cations in the diffuse layer is decreasing exponentially with increasing distance from the mineral surface (see Fig. \ref{fig:concentration_ions_edl}, after \citet{Berktold_2005}). But the formation of EDLs is not the only cause for Induced Polarization effects in minerals.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../../code/plots/illustration_sip/concentration_ions_edl.png}
    \caption{Concentration of Anions and Cations in the Electric Double Layer (EDL) plotted over the distance from the mineral surface, after \citet{Berktold_2005}.}
    \label{fig:concentration_ions_edl}
\end{figure}

The second cause is the mineral's lithographic structure and hence the structure of its pore space. The pore space determines the distribution of ions in the electrolytic pore fill. At pore constrictions, charge accumulations can be observed that build up and relax delayed to the external current. 
Zones of such charge accumulation, where the pores are quite narrow and prevent the charges from flowing freely, are called "active" zones. In contrast, zones with wide pores where the ions can flow unhindered through the pore space are called "passive" zones. If the external electric field $E_\mathit{ext}$ changes abruptly with frequencies $\omega \gg 10\,\si{Hz}$ the charges can not be completely separated to form a full EDL. Consequently, the diffuse layer is deformed. The charge distribution in the mineral can not reach a new equilibrium state. Hence the full polarization can not be observed at high frequencies. In this thesis, high frequencies $\omega \in [10^2\,\si{Hz}, 10^5\,\si{Hz}]$ are used to observe the IP-effects of ice. As the relaxation frequency of ice is $\sim 7\,\si{kHz}$, these effects are not observable at significantly smaller frequencies. The phase maximum is at $\omega = \tau^{-1}$ corresponding to $\sim 40\,\si{kHz}$. 

\subsection{Spectral Models}
\subsubsection{Cole-Cole-Model (CCM)}
A model to describe the frequency-dependent polarization behavior of the subsoil is the Cole-Cole model, which is widely used for the two-dimensional inversion of field data. This model is derived from a replacement circuit accounting for the capacitive properties of the IP-effects. The replacement circuit consists of a resistor describing the controls low-frequency DC behavior and a frequency-dependent impedance controlling the relaxation \citep{Grimm_2015}.

The the frequency-dependent conductivity $\sigma(\omega)$ for the CCM used in this thesis is given by
\begin{equation}\label{eq:ccm}
    \sigma(\omega) = \frac{1}{\rho_\mathrm{DC}} + i\omega\epsilon_0\qty[\epsilon_\mathrm{HF} + \frac{\epsilon_\mathrm{DC}- \epsilon_\mathrm{HF}}{1+ (i\omega\tau)^c}] 
\end{equation}
with $\rho_\mathrm{DC}$ being the DC resistivity, $\epsilon_\mathrm{HF}$ the high-frequency limit of the relative permittivity, $\epsilon_\mathrm{DC}$ the corresponding low-frequency limit, $\tau$ the relaxation time and $c$ the relaxation exponent \citep{mudler_2021}.

\subsubsection{Zorin-Ageev Model (Ice Content Model)}
The frequency-dependent conductivity $\sigma(\omega)$ for the Zorin-Ageev model is defined by
\begin{equation}\label{eq:zorin_ageev}
  \sigma^k(\omega) = (1-\alpha)\sigma_m^k + \alpha\sigma_i^k, 
\end{equation}
where the index $m$ is used for the matrix material and $i$ for ice.
The conductivity of the matrix material is given by  
\begin{equation}
    \sigma_m = \frac{1}{\rho_m} + i\omega\epsilon_0\epsilon_m,
\end{equation}
where $\rho_m$ denotes the DC resistivity and $\epsilon_m$ the relative permittivity of the matrix material.
The conductivity of ice can be described by 
\begin{equation}
    \sigma_i = \frac{1}{\rho_{\mathrm{DC},\,i}} + i\omega\epsilon_0\qty[\epsilon_{\mathrm{HF},\,i} + \frac{\epsilon_{\mathrm{DC},\,i}- \epsilon_{\mathrm{HF},\,i}}{1+ (i\omega\tau_i)^c}],
\end{equation}
where $\rho_{\mathrm{DC},\,i}$ is the DC resistivity of ice, $\epsilon_{\mathrm{DC},\,i}$ and $\epsilon_{\mathrm{HF},\,i}$ the low- and high-frequency limits of the relative permittivity of ice and $\tau_i$  is the relaxation time of ice \citep{mudler_2021}.
The parameter $k$ represents the pore-scale structure of the material at hand. The exponent $k$ can be bounded to values $k\in[0, 0.5]$ for most field measurements \citep{Zorin_2017}. \citet{Zorin_2017} describe the observation that the parameter $k$ has a strong impact on the resulting spectrum calculated using Eq. \eqref{eq:zorin_ageev}.  

\begin{figure}
    \centering
    \includegraphics[width=0.8 \textwidth]{../../code/plots/analysis/spectrum_comparison_alpha.png}
    \caption{Zorin-Ageev modelled absolute value and phase spectrum for 11 equidistant ice contents in the interval $[0, 1]$. The remaining Zorin-Ageev parameters are set to $\epsilon_m = 30$, $\sigma_m = 3.33 \cdot 10^{-4} \,\si{\frac{S}{m}}$, $k=0.5$, $\tau_i = 2.3 \cdot 10^{-5} \,\si{s}$, $\sigma_{i,\,\mathrm{DC}} = 1 \cdot 10^{-7} \,\si{\frac{S}{m}}$.}
    \label{fig:ice_content_comparison_intro}
\end{figure}

Fig. \ref{fig:ice_content_comparison_intro} illustrates Zorin-Ageev model spectra generated using Eq. \eqref{eq:zorin_ageev} for varied ice contents $\alpha$. 