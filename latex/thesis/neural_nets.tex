% !TEX root = main.tex
\section{Neural Networks}
This section is mainly based on \citet{Goodfellow_2016}.

\subsection{Structure}
A Neural Network consists of multiple layers: The input layer where the data is presented to the Neural Network, one or more hidden layers, and an output layer through which the Neural Network passes its output. This output is dependent on the given set of input parameters. 

The neural network's free parameters are adjusted in the training process of the model. In training the model tries to reproduce the given target values $\vb{y}_{\mathrm{target}}$ from the corresponding input values $\vb{v}^(0)$. The training results are then evaluated on a second test data set, containing data not used in training. This evaluation on data points unknown to the mode is to investigate whether the model can abstract the dependencies it found between the input and output values of the training data set to new inputs.

Each layer of the neural network consists of a certain number of nodes, the neurons. In a dense layer, each node of the $i$-th layer is connected with each node of the $i+1$-th layer. These connections are illustrated in Fig. \ref{fig:structure_neural_net}. 

The input value $v^{(i+1)}_k$ of the $k$-th neuron in the $i+1$-th layer is the weighted sum of all outputs of the neurons in the $i$-th layer, given by
\begin{equation}\label{eq:weight}
    v^{(i+1)}_k = \sum_{j} W^{(i)}_{kj} f(v^{(i)}_j) + b^{(i)}_k,
\end{equation}    
with $f(v^{(i)}_j)$ being the value passed to the $i+1$-th layer from the $j$-th Neuron of the $i$-th layer, $W^{(i)}_{kj}$ being the weight of the connection between the $k$-the neuron in the $i+1$-th and $j$-th neuron in the $i$-th layer. $b^{(i)}_k$ denotes the bias that is often introduced to fit non-centered data. 

If the weights $W^{(i)}_{kj}$ are compactly written using a weight matrix $\vb{W}^{(i)}$, Eq. \eqref{eq:weight} can be written as a matrix-vector multiplication
\begin{equation}\label{eq:weight_vector}
    \vb{v}^{(i+1)} = \vb{W}^{(i)} f(\vb{v}^{(i)}) + \vb{b}^{(i)}.
\end{equation}    
\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../presentation/neural_network.png}
    \caption{Example layer structure of a Neural Network, after \footnotesize{towardsdatascience.com \\\vspace{-0.2cm}Everything you need to know about Neural Networks and Backpropagation}}
    \label{fig:structure_neural_net}
\end{figure}

\subsection{Activation Functions}
The function $f$ used to calculate the value a node passes to the next layer, is called \emph{Activation Function}. The value $v^{(i)}_j$ obtained from Eq. \eqref{eq:weight} is put into a function $f$ and $f(v^{(i)}_j)$ is passed to the $i+1$-th layer. Some commonly used activation function are the Rectified Linear Unit (ReLU) given by
\begin{equation}
    \mathrm{ReLU} = \max\{x, 0\}, 
\end{equation}
the Sigmoid Curve given by
\begin{equation}
    \sigma(x) = \frac{1}{1+ e^{-t}} = \frac{e^t}{1 + e^t}
\end{equation}
and the tangent hyperbolicus $\tanh(x)$ (see Fig. \ref{fig:activation_functions}). In this thesis the ReLU will be used as activation function. 

The activation function is necessary for the model to fit non-linear functional dependencies between the input and target values. Without activation functions, the Neural Network would fit a multi-dimensional Linear Function to the data set, which is not intended in general. 

\begin{figure}
    \vspace{-4cm}
    \centering
    \subfloat[]{
        \includegraphics[width=0.70\textwidth]{../../code/plots/illustration_neural_networks/ReLU.png}
    }\\
    \subfloat[]{
        \includegraphics[width=0.70\textwidth]{../../code/plots/illustration_neural_networks/tanh.png}
    }\\
    \subfloat[]{
        \includegraphics[width=0.70\textwidth]{../../code/plots/illustration_neural_networks/sigmoid_curve.png}
    }
\caption{Output values vs. input values for commonly used Activation Functions.}
\label{fig:activation_functions}
\end{figure}

\subsection{Gradient Descent}
Using Eq. \ref{eq:weight_vector}, the complete Neural Network with $n$ layers can be written as a chained function
\begin{multline}
    M(\vb{v}^{(0)}, \vb{W}^{(0)}, \dots, \vb{W}^{(n)}, \vb{b}^{(0)}, \dots, \vb{b}^{(n)}) \\= \vb{W}^{(n)} f(\vb{W}^{(n-1)} f(\dots(f(\vb{W}^{(0)}\vb{v}^{(0)} + \vb{b}^{(0)})) + \dots ) + \vb{b}^{(n-1)}) + \vb{b}^{(n)}.
\end{multline}
To train a Neural Network the cost function $\mathcal{C}$ that measures the resiudal between the models predicted output values $\vb{y}_\mathrm{pred}$ and the target values $\vb{y}_\mathrm{target}$ passed into the models training process, has to be minimized. If the RMSE is the loss metric to be minimized, the cost function can be written as 
\begin{multline}
    \mathcal{C}(\vb{v}^{(0)}, \vb{W}^{(0)}, \dots, \vb{W}^{(n)}, \vb{b}^{(0)}, \dots, \vb{b}^{(n)}) \\= \sqrt{\sum_j \qty(M_j(\vb{v}^{(0)}, \vb{W}^{(0)}, \dots, \vb{W}^{(n)}, \vb{b}^{(0)}, \dots, \vb{b}^{(n)}) - \vb{y}_{\mathrm{target},\,j})^2}.
\end{multline}
The value of the cost function is often referred to as \emph{Loss}.

The parameters that can be optimitzed are stored in the weight matrices $\vb{W}^{(i)}$. For the optimization of the parameters all weights are combined into a parameter vector $\boldsymbol{\theta}$. To minimize the cost function $C$ for all samples the norm of the average gradient $\vb{g}$ of the cost function with respect to the parameter vector $\boldsymbol{\theta}$, given by
\begin{equation}
    \vb{g} = \frac{1}{n} \sum_{i=1}^{n} \pdv{\boldsymbol{\Theta}} \mathcal{C}(\vb{v}^{(0)}, \vb{W}^{(0)}, \dots, \vb{W}^{(n)}),
\end{equation} 
with $n$ being the number of samples.

In this thesis, the Adam Optimizer is used to adjust these parameters. The Adam optimizer is based on Stochastic Gradient Descent (SGD) (see Alg. \ref{alg:sgd}, after \citet{Goodfellow_2016}). 

One iteration in the \texttt{while}-loop of Algorithm \ref{alg:sgd} is an \emph{Epoch}. In Alg. \ref{alg:sgd} $k$ denotes the current training epoch. The maximum number of epochs $k_\mathit{max}$ determines how many epochs the model is trained until the training loop terminates. The other termination condition for the training loop is that the Gradient Descent has converged, and the value of the cost function $C$ is smaller than a specified tolerance (see Alg. \ref{alg:sgd}).

In each epoch, multiple smaller random subsets of $m$ samples taken from the training data set (\emph{Batches}) are used for the optimization, with $m$ being called the \emph{Batch Size}. Training the model on batches can help to avoid overfitting. Models trained on small batches tend to be less stable during the training, whereas larger batches are more stable as the batches are more similar to each other and are less prone to statistical effects. The optimal batch size is determined by training models with different batch sizes and comparing the loss on the test data set. The process of systematically trying multiple values for the algorithm-specific parameters of an ML-Algorithm is called \emph{Hyperparameter-Tuning}. One strategy for tuning more than one hyperparameter is to test all parameter value combinations. This strategy is called Grid Search.

The parameter vector $\boldsymbol{\theta}$ is updated in each epoch. The parameter vector $\boldsymbol{\theta}_{k+1}$ in the $k+1$-the epoch is defined by
\begin{equation}
    \boldsymbol{\theta}_{k+1} = \boldsymbol{\theta}_k - \vb{g}.
\end{equation}
This basic update can be modified by introducing the learning rate $\epsilon$. The learning rate determines how far the model follows the gradient $\vb{g}$. The parameter vector $\boldsymbol{\theta}_{k+1}$ in the $k+1$-the epoch is then given by
\begin{equation}
    \boldsymbol{\theta}_{k+1} = \boldsymbol{\theta}_k - \epsilon \vb{g}.
\end{equation}
In general, small learning rates mean that the training is slower, but in most cases, more stable. Models trained with very small learning rates can easily get stuck in a local minimum of the cost function $\mathcal{C}$. If the learning rate is too high, this could lead to the problem that the model is overshooting the global minimum of the cost function $\mathcal{C}$. The learning rate is also optimized in the Hyperparameter-Tuning. 

The last parameter that should be discussed in preparation for the Hyperparameter-Tuning in chapter \ref{chap:neural_nets_synthetic} is the regularization parameter $r$.
The cost function $\mathcal{C}$ is modified by adding a regularization term proportial to $\norm{\boldsymbol{\theta}}_2^2$. Hence, using $L^2$-regularization the modified cost function can be written as
\begin{equation}
    \tilde{\mathcal{C}} = \frac{r}{2} \norm{\boldsymbol{\theta}}_2^2 + \mathcal{C} = \frac{r}{2} \boldsymbol{\theta} \vdot \boldsymbol{\theta} + \mathcal{C}. 
\end{equation}
The gradient $\tilde{\vb{g}}$ of $\tilde{\mathcal{C}}$ is given by
\begin{equation}
    \tilde{\mathcal{C}} = r \boldsymbol{\theta} + \vb{g}.
\end{equation}
This gradient is used for the update of the parameter vector $\boldsymbol{\theta}$ in Alg. \ref{alg:sgd}.
$L^2$-Regularization is used to shrink the weights of features that are not particularly strongly correlated with the model's target values, as the model will primarily follow the gradient in those directions, where the movement will significantly increase the gradient. Hence a regularized model is capable of finding the most important features. Using regularization is another strategy to avoid overfitting, as it supports the model in finding the more general dependencies between the input and target values.   

\begin{algorithm}
    \caption{Stochastic Gradient Descent with Regularization, after \citet{Goodfellow_2016}.}
    \label{alg:sgd}
    \begin{algorithmic}
        \REQUIRE Learning rate $\epsilon$
        \REQUIRE Maximum Number of epochs $k_\mathit{max}$
        \REQUIRE Regularization Parameter $r$
        \REQUIRE Initialized Parameter Vector $\boldsymbol{\theta}$ containing the parameters of all Weight Matrices $\vb{W}^{(i)}$ and the biases $\vb{b}^{(i)}$, typically this is done randomly
        \REQUIRE Training data  $\vb{v}^{(0)}$ and $\vb{y}_\mathrm{target}$
        \STATE $k \leftarrow 1$ \COMMENT{Counter for epochs}
        \WHILE{$\mathcal{C} >$ tolerance}
        \STATE Sample a batch of $m$ training samples of input values $\vb{v}^{(0)}_1, \dots, \vb{v}^{(0)}_m$ and their corresponding target values $\vb{y}_\mathrm{target}$ 
        \STATE $\tilde{\vb{g}} \leftarrow r \boldsymbol{\theta} + \frac{1}{m} \sum_{i=1}^m \pdv{\boldsymbol{\Theta}} \mathcal{C}(\vb{v}^{(0)}, \vb{W}^{(0)}, \dots, \vb{W}^{(n)})$
        \STATE Update $\boldsymbol{\theta} \leftarrow \boldsymbol{\theta} - \epsilon \tilde{\vb{g}}$
        \STATE $k \leftarrow k + 1$
        \IF{$k = k_\mathit{max}$} 
        \STATE \textbf{break}
        \ENDIF
        \ENDWHILE
    \end{algorithmic}
\end{algorithm}

\section{$k$-Nearest Neighbor Regression}
$k$-nearest Neighbor Regression is Machine Learning-Algorithm that, in contrast to the Neural Networks discussed in the last section, is not fitted in multiple epochs but in a single step.

The regressor uses a weighted or unweighted mean of the target values of the $k$ closest neighbors for a data point $\vb{P}$ in the training data set to predict the target value for that particular data point $\vb{P}$.

Here the $k$-nearest Neighbor Algorithm is implemented using the $p$-norm $\norm{\cdot}_p$, so the distance between two points $\vb{P}$ and $\vb{Q}$ is given by
\begin{equation}\label{eq:p_norm}
	\norm{\vb{P} - \vb{Q}}_p = \sqrt[p]{\sum_i \qty(P_i - Q_i)^p}. 
\end{equation}

The mean of the $k$-nearest neighbors of that point is given by 
\begin{equation}\label{eq:unweighted_mean}
    \vb{y}_{\mathrm{target},\,\vb{P}} = 
    \frac{1}{k} \sum_{i \in \text{$k$ neigbors}} \vb{y}_{\mathrm{target},\,\vb{Q_i}}.
\end{equation}
Another way to calculate the prediction $\vb{y}_{\mathrm{target},\,\vb{P}}$ is to use the weighted mean of the target values of the $k$ neighbors. Using this strategy $\vb{y}_{\mathrm{target},\,\vb{P}}$ is given by
\begin{equation}\label{eq:weighted_mean}
    \vb{y}_{\mathrm{target},\,\vb{P}} = \sum_{i \in \text{$k$ neigbors}} \norm{\vb{P} - \vb{Q_i}} 
    \sum_{i \in \text{$k$ neigbors}} \frac{1}{\norm{\vb{P} - \vb{Q_i}}} \vb{y}_{\mathrm{target},\,i}.    
\end{equation} 


