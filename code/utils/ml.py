from typing import List, Union
import time
import functools
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pickle
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPRegressor
from sklearn.neighbors import KNeighborsRegressor
from utils.etl import *
from utils.plot import *

full_model_names = {'k_neigh': '$k$-nearest neighbor Regressor fitted on scalar spectral features', 
                    'features': 'Neural Network trained on scalar spectral features', 
                    'ccm': 'Neural Network trained on full CCM phase spectra with 100 equidistant frequecies over the frequency range of $\Omega$', 
                    'zorin_ageev': 'Neural Network trained on full Zorin-Ageev phase spectra with 100 equidistant frequecies over the frequency range of $\Omega$'
                    }

def timer(func):
    @functools.wraps(func)
    def timer_wrapper(*args, **kwargs):
        start_time = time.time()
        value = func(*args, **kwargs)
        end_time = time.time()
        run_time = end_time - start_time
        print(f"Finished `{func.__name__}` in {run_time:.5f} s.")
        return value

    return timer_wrapper


def eval_line_plots(eval_frame: pd.DataFrame, model_name: str, data_subset: str):
    eval_frame['alpha_real_round'] = eval_frame['alpha_real'].round(0)

    alphas_frame = eval_frame.groupby('alpha_real_round').agg(alpha_pred_max=('alpha_pred', 'max'),
                                                              alpha_pred_min=('alpha_pred', 'min'),
                                                              alpha_pred_mean=('alpha_pred', 'mean'))
    rmse_frame = eval_frame.groupby('alpha_real_round').agg(rmse_max=('AE', 'max'),
                                                            rmse_min=('AE', 'min'),
                                                            rmse_mean=('AE', 'mean'))
    setup_plot(r'$\alpha_\mathrm{real}$ (%)', r'$\alpha_\mathrm{pred}$ (%)',
               r'Predicted value for $\alpha$ by $\alpha_\mathrm{real}$')
    for col in alphas_frame.columns:
        plt.plot(alphas_frame.index, alphas_frame[col], label=col.split('_')[-1])
    plt.legend()
    plt.savefig(f'{path_plots}/neural_networks/alpha_pred_by_alpha_{model_name}_{data_subset}.png', dpi=1000)
    plt.show()

    setup_plot(r'$\alpha_\mathrm{real}$ (%)', r'Absolute Error',
               r'Absolute error by $\alpha_\mathrm{real}$')
    for col in rmse_frame.columns:
        plt.plot(rmse_frame.index, rmse_frame[col], label=col.split('_')[-1])
    plt.legend()
    plt.savefig(f'{path_plots}/neural_networks/rmse_by_alpha_{model_name}_{data_subset}.png', dpi=1000)
    plt.show()


def eval_scatter_plots(train, test, model_name):
    identity = lambda x: x
    x = np.linspace(0, 100, 10)

    # Comparison plot for test data
    fig = plt.figure(frameon=False)
    setup_plot(r'$\alpha_\mathrm{real}$ (%)', r'$\alpha_\mathrm{pred}$ (%)',
               'Predictions on test data (out of sample)')
    plt.plot(x, identity(x), color='orange')
    plt.scatter(test['alpha_real'][:100], test['alpha_pred'][:100])
    plt.savefig(f'{path_plots}/neural_networks/scatter_test_{model_name}.png', dpi=1000)
    plt.show()  

    # Comparison plot for training data
    setup_plot(r'$\alpha_\mathrm{real}$ (%)', r'$\alpha_\mathrm{pred}$ (%)',
               'Predictions on training data')
    
    plt.plot(x, identity(x), color='orange')
    plt.scatter(train['alpha_real'][:100], train['alpha_pred'][:100])
    plt.savefig(f'{path_plots}/neural_networks/scatter_train_{model_name}.png', dpi=1000)
    plt.show()


def plot_learning_history(regressor, model_name: str):
    setup_plot('epoch', 'loss (AE)', 'Loss by epochs')
    plt.plot(regressor.loss_curve_)
    plt.savefig(f'{path_plots}/neural_networks/learning_hist_{model_name}.png', dpi=1000)
    plt.show()


def absolute_error(y_real, y_pred):
    return np.abs(y_real - y_pred)


def squared_error(y_real, y_pred):
    return (y_real - y_pred) ** 2

def get_rmse_from_squared_error(squared_error: pd.DataFrame) -> float:
    mean_squared_error = squared_error.mean()
    return np.sqrt(mean_squared_error)

def create_frame_with_error_metrics(regressor, x, y_real, metrics: List[str]):
    # Reset index
    y_real = y_real.reset_index(drop=True)

    frame_with_errors = pd.DataFrame(columns=['alpha_real', 'alpha_pred'])
    frame_with_errors['alpha_pred'] = np.exp(pd.Series(regressor.predict(x))) * 100  # Make predictions
    frame_with_errors['alpha_real'] = np.exp(y_real) * 100

    metric_func_mapping = {'AE': absolute_error, 'SE': squared_error}
    for metric in metrics:
        frame_with_errors[metric] = metric_func_mapping[metric](frame_with_errors['alpha_real'],
                                                                frame_with_errors['alpha_pred'])
    score = regressor.score(x, y_real)
    return frame_with_errors, score

def plot_prediction_for_measurements(y_pred, model_name):
    alpha_nn = np.array(y_pred/100).reshape(26,30)
    print(f"Maximal ice content: {np.max(alpha_nn):.2%}")
    print(f"Minimal ice content: {np.min(alpha_nn):.2%}")
    alpha_nn[alpha_nn > .45] = .45
    setup_two_dimensional_profile(np.transpose(alpha_nn), r'$\alpha$', cmap_segements=15)
    plt.savefig(f'{path_plots}/neural_networks/neural_network_fit_{model_name}.png', bbox_inches='tight', dpi=1000)
    plt.show()

def plot_difference_picture_for_measurements(y_diff, model_name):
    alpha_diff = np.array(y_diff/100).reshape(26,30)
    setup_two_dimensional_profile(np.transpose(alpha_diff), r'$\Delta\alpha$', cmap_segements=15)
    plt.savefig(f'{path_plots}/neural_networks/neural_network_fit_{model_name}_diff.png', bbox_inches='tight', dpi=1000)
    plt.show()

def export_model(regressor, filename: str):
    """
    Pickle model and write to specified file
    
    :param regressor: The regressor to export
    :param str filename: Path/filename of the file to write pickeled object to

    """
    with open(filename, 'wb') as file: 
        pickle.dump(regressor, file)
    
    
@timer
def fit_model(model_name: str, regressor, x_train, y_train, param_grid: dict = None):
    if param_grid is not None:
        regressor = GridSearchCV(regressor, param_grid, verbose = 0, cv = 3)
    
    regressor.fit(x_train, y_train)
    if param_grid is not None:
        cv_results = pd.DataFrame(regressor.cv_results_)
        columns_to_select = [f"param_{key}" for key in param_grid.keys()]
        columns_to_select.extend(['mean_fit_time', 'mean_test_score', 'rank_test_score'])
        col_shortener = lambda x: x.replace('test_', '') if 'test_score' in x else '_'.join(x.split('_')[1:])
        cv_export = cv_results[columns_to_select].rename(col_shortener, axis=1)
        cv_export.to_latex(f"{path_tables}/{model_name}_cross_valid.tex", index=False, caption=f'Cross validation results and parameters for {full_model_names[model_name]}', label=f"tab:{model_name}_cross_valid", longtable=True, float_format=lambda x: '%.3f' % x)
        return regressor.best_estimator_
    
    return regressor


def create_combined_evaluation_table_for_data_set(train, train_score, test, test_score, measured = None, measured_score = None) -> pd.DataFrame:
    metrics = {}
    metrics['train'] = [train_score]
    metrics['test'] = [test_score]
    
    if measured_score is not None:
        metrics['field data'] = [measured_score]

    metrics['train'].append(get_rmse_from_squared_error(train['SE']))
    metrics['test'].append(get_rmse_from_squared_error(test['SE']))
    if measured_score is not None:
         metrics['field data'].append(get_rmse_from_squared_error(measured['SE']))
    evaluation_frame = pd.DataFrame.from_dict(metrics, orient='index', columns=['score', 'RMSE'])
    return evaluation_frame


@timer
def eval_model(model_name: str, regressor, x_train, y_train, x_test, y_test, x_measured = None, y_measured = None) -> pd.DataFrame:
    metrics = ['AE', 'SE']

    if (x_measured is not None and y_measured is None) or (x_measured is None and y_measured is not None):
        raise ValueError('Only one of `x_measured` and `y_measured` specified.')
    
    # Create DataFrames
    errors_for_evaluation = []
    train, train_score = create_frame_with_error_metrics(regressor, x_train, y_train, metrics)
    test, test_score = create_frame_with_error_metrics(regressor, x_test, y_test, metrics) 
    errors_for_evaluation.extend([train, train_score, test, test_score])

    if x_measured is not None:
        measured, measured_score = create_frame_with_error_metrics(regressor, x_measured, y_measured, metrics)
        errors_for_evaluation.extend([measured, measured_score])

    eval_table = create_combined_evaluation_table_for_data_set(*errors_for_evaluation)
    eval_table.to_latex(f'{path_tables}/_errors_{model_name}.tex', 
                        formatters=formatters,
                        escape = False,
                        caption=f"Errors of the {full_model_names[model_name]} on all three data sets.",
                        label = f"tab:errors_{model_name}"
    )

    eval_table.iloc[:-1].to_latex(f'{path_tables}/_errors_{model_name}_small.tex', 
                    formatters=formatters,
                    escape = False,
                    caption=f"Errors of the {full_model_names[model_name]} on the train and test data sets.",
                    label = f"tab:errors_{model_name}_small"
    )
            
    eval_line_plots(test, model_name, 'test')
    eval_line_plots(measured, model_name, 'measured')
    eval_scatter_plots(train, test, model_name)
    if x_measured is not None:
        plot_prediction_for_measurements(measured['alpha_pred'], model_name)
        plot_difference_picture_for_measurements(measured['alpha_pred'] - measured['alpha_real'], model_name)
    
    return eval_table

def get_func_dependency_frame(regressor, feature, features, test, steps: int = 100):
    # Initialize DataFrame
    func_dependency_frame = pd.DataFrame(columns=features)
    func_dependency_frame[feature] = np.linspace(test[feature].min(), test[feature].max(), steps)
    for feature_ in features:
        if feature_ == feature:
            continue
        else:
            func_dependency_frame[feature_] = test[feature_].mean()
    func_dependency_frame['prediction'] = np.exp(pd.Series(regressor.predict(func_dependency_frame))) * 100
    return func_dependency_frame

def vary_features(regressor, features, x_test): 
    for feature in features:
        func_dependency_frame = get_func_dependency_frame(regressor, feature, features, x_test)
        setup_plot(feature, r'$\alpha_\mathrm{pred}$ (%)')
        plt.plot(func_dependency_frame[feature], func_dependency_frame['prediction'])
        plt.savefig(f'{path_plots}/neural_networks/feature_variation_{feature}.png', dpi=1000)
        plt.show()

def get_table_of_layer_sizes(regressor):
    hidden_layer_sizes = regressor.get_params()['hidden_layer_sizes']
    hidden_layers_pd =  pd.DataFrame(hidden_layer_sizes, columns=[r'Nodes $l_i$']).rename_axis(r"Hidden Layer $i$").reset_index()
    hidden_layers_pd[r"Hidden Layer $i$"] += 1
    hidden_layers_pd.to_latex(f'{path_tables}/_num_nodes_neural_nets.tex', 
                        escape = False,
                        caption=f"Number of Neurons in each Hidden Dense Layer $i$ of the Multilayer Perceptron (MLP) for all three approaches using Neural Networks.",
                        label = f"tab:num_nodes_neural_nets", 
                        index = False
    )
    return hidden_layers_pd

def get_num_of_free_parameters(regressor: MLPRegressor) -> int:
    return sum([coeffs.size for coeffs in regressor.coefs_])