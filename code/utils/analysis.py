from typing import Tuple
import matplotlib as plt
from matplotlib.pyplot import get
import pandas as pd
from utils.plot import *
from utils.sip import add_ccm_to_frame, add_zorin_ageev_spectrum_to_frame, ccm_from_parameters, fit_from_zorin_ageev_to_ccm, zorin_ageev_spectrum_fitting, zorin_ageev_spectrum_from_parameters
from utils.sip_config import *
from utils.etl import get_latex_string_with_unit, path_plots, formatters
import seaborn as sb

def reformat_row(row: pd.DataFrame) -> Tuple[str, float]:
    latex_param = get_latex_string_with_unit(row['parameter'])
    fomatted_value = formatters[latex_param](row['value'])
    return latex_param, fomatted_value

def plot_example_spectra(frame: pd.DataFrame, num: int = 3, spectra_per_plot: int = 1, save: bool = False) -> None:
    # Choose correct number of samples
    frame = frame.head(num * spectra_per_plot)
    for index, row in frame.iterrows():
        spectrum = row['spectrum_zorin_ageev']
        if index % spectra_per_plot == 0:
            axs = setup_spectral_plot()
        # Put spectra
        axs[0].plot(omega, spectrum[:100])
        if spectra_per_plot == 1:
            parameters = row.T.iloc[:-1].reset_index().rename({'index': 'parameter', index: 'value'}, axis=1)
            parameters[['parameter', 'value']] = parameters.apply(reformat_row, axis=1).to_list()
            table = axs[0].table(cellText=parameters.values, colLabels=['parameter', 'value'], loc='upper left', colWidths=[.15, .15])
        table.auto_set_font_size(False)
        table.set_fontsize(12)
        table.scale(1.25, 1.25)
        axs[1].plot(omega, spectrum[100:])  
        if index % spectra_per_plot == spectra_per_plot - 1:
            if save:
                plot_num = index // spectra_per_plot
                plt.tight_layout()
                plt.savefig(f'{path_plots}/analysis/spectrum_example_{plot_num}.png', dpi = 1000)
            plt.show()

def plot_comparing_spectra(frame: pd.DataFrame, num: int = 3) -> None:
    for index, row in frame.head(num).iterrows():
        axs = setup_spectral_plot()
        spectrum_zorin_ageev = row['spectrum_zorin_ageev']
        spectrum_ccm = row['spectrum_ccm']
        # Put spectra
        axs[0].plot(omega, spectrum_zorin_ageev[100:])
        axs[0].plot(omega, spectrum_ccm[100:])        
        axs[1].plot(omega, spectrum_zorin_ageev[:100])
        axs[1].plot(omega, spectrum_ccm[:100])
        plt.savefig(f'{path_plots}/analysis/spectrum_example_{index}_zorin_ageev_ccm.png', dpi = 1000)    
 
def create_rmse_correlation_plots(frame_with_rmse: pd.DataFrame, param_to_correlate: str, round_digits:int = 2, greek=False):
    try:
        frame_with_grouped_rmse = frame_with_rmse[[param_to_correlate, 'RMSE']].copy()
    except KeyError as err:
        raise ValueError('RMSE and/or {param_to_correlate} not in the columns of the investigated DataFrame.')
    
    # Grouping 
    frame_with_grouped_rmse[f'{param_to_correlate}_round'] = frame_with_grouped_rmse[param_to_correlate].round(round_digits)
    frame_with_grouped_rmse = frame_with_grouped_rmse.groupby(f'{param_to_correlate}_round').mean().reset_index()
    
    if greek:
        label = f"$\\{param_to_correlate}$"
    else:
        label = f"${param_to_correlate}$"
    
    setup_plot(label, 'RMSE')
    plt.scatter(frame_with_grouped_rmse[f'{param_to_correlate}_round'], frame_with_grouped_rmse['RMSE'])
    plt.savefig(f'{path_plots}/analysis/rmse_by_{param_to_correlate}_synthetic.png', dpi=800)
    plt.show()
    
    print(f"""correlation coefficient between {param_to_correlate} and RMSE : 
    {frame_with_grouped_rmse[param_to_correlate].corr(frame_with_grouped_rmse['RMSE']):.4f}""")
    print(f"""correlation coefficient between {param_to_correlate} and RMSE (with rounded values): 
    {frame_with_grouped_rmse[f'{param_to_correlate}_round'].corr(frame_with_grouped_rmse['RMSE']):.4f}""")

def calc_rmse(row: pd.DataFrame) -> float:
    spectrum, spectrum_ccm = row
    phase_spectrum = spectrum[100:] 
    phase_spectrum_ccm = spectrum_ccm[100:] 
    residuals = (phase_spectrum_ccm - phase_spectrum)
    rmse_ = (np.sum(residuals ** 2) / (residuals.size)) ** 0.5
    
    return rmse_

def add_rmse_to_fitted_data(frame_with_fitted_spectrum: pd.DataFrame) -> pd.DataFrame:
    frame_with_rmse = frame_with_fitted_spectrum.copy()
    frame_with_rmse['RMSE'] = frame_with_rmse[['spectrum_zorin_ageev', 'spectrum_ccm']].apply(calc_rmse, axis=1)
    return frame_with_rmse

def plot_parameter_histograms(frame: pd.DataFrame) -> None:
    for i, col in enumerate(frame.columns):
        try:
            # Check if code is executed after the 
            if col == 'spectrum':
                continue
            latex_string = latex_mapping[col] 
            latex_string_with_unit = get_latex_string_with_unit(col) 
            setup_histogram_plot(frame, col, latex_string, latex_string_with_unit)
            plt.savefig(f'{path_plots}/analysis/hist_synthetic_data_{i}.png', dpi=800)
            plt.show()
        except Exception as err:
            raise ValueError("You added a column to the DataFrame that isn't expected")

def get_spectra_for_k(k: float):
    spectrum_zorin_ageev = zorin_ageev_spectrum_from_parameters([0.4, 10, 3e-4, k, 3e-5, 1e-6], False)
    spectrum_ccm = fit_from_zorin_ageev_to_ccm({'spectrum_zorin_ageev': spectrum_zorin_ageev, 'sigma_i_DC': 1e-6, 'tau_i': 3e-5})
    return spectrum_zorin_ageev, spectrum_ccm
    
def compare_plots(k: float):
    spectrum_zorin_ageev, spectrum_ccm = get_spectra_for_k(k)
    buffer = pd.DataFrame(columns = ['spectrum_zorin_ageev', 'spectrum_ccm'], index=range(1))
    buffer.iloc[0, :] = spectrum_zorin_ageev, spectrum_ccm
    plot_comparing_spectra(buffer)
    plt.tight_layout()
    plt.savefig(f'{path_plots}/analysis/compare_ccm_two_comp_k={k}.png', dpi=1000)
    plt.show()

def get_spectrum_for_alpha(alpha):
    # Set example values for free spectral parameters
    rho_m = 3000
    sigma_m = 1/rho_m
    epsilon_m = 30
    tau_i = 2.3e-5
    k = .5

    sigma_m_tilde = 1/rho_m + 1j * omega * EPSILON_0 * epsilon_m
    sigma_i_tilde = 1/rho_i_DC + 1j * omega * EPSILON_0 * (epsilon_i_hf + (epsilon_i_DC - epsilon_i_hf)/(1 + (1j * omega * tau_i) ** c))
    sigma = ((1 - alpha) * sigma_m_tilde ** k + alpha * sigma_i_tilde ** k) ** (1/k)
    
    return sigma

def plot_spectra_for_different_ice_contents(N: int = 11):
    plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.jet(np.linspace(0, 1, N)))
    axs = setup_spectral_plot()
    alpha_vals = np.linspace(0, 1, N)
    spectra = [get_spectrum_for_alpha(alpha) for alpha in alpha_vals]
    for i, alpha in enumerate(alpha_vals):
        axs[0].plot(omega, np.abs(spectra[i]), label=f"{alpha:.1%}")
        axs[1].plot(omega, np.angle(spectra[i]))
    plt.tight_layout()
    axs[0].legend(title = r"ice content $\alpha$", ncol=2)
    plt.savefig(f'{path_plots}/analysis/spectrum_comparison_alpha.png', dpi=800)
    plt.show()

    # Back to default color cycle
    plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.tab10.colors)

def plot_spectra_for_different_values_of_k(N: int = 11):
    plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.jet(np.linspace(0, 1, N)))
    axs = setup_spectral_plot()
    k_vals = np.linspace(0.1, 0.5, N)
    spectra = [get_spectra_for_k(k)[0] for k in k_vals]
    for i, k in enumerate(k_vals):
        axs[0].plot(omega, spectra[i][:100], label=f"{k:.4f}")
        axs[1].plot(omega, spectra[i][100:])
    plt.tight_layout()
    axs[0].legend(title = r"$k$", ncol=2)
    plt.savefig(f'{path_plots}/analysis/spectrum_comparison_k.png', dpi=800)
    plt.show()

    # Back to default color cycle
    plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.cm.tab10.colors)

def calc_spectral_properties(row, spectral_type = 'zorin_ageev'):
    global i
    i += 1
    phase_spectr = row[f'spectrum_{spectral_type}'][100:]
    if i % 50000 == 0:
        print(f'{i} rows finished.')
    return np.min(phase_spectr), np.max(phase_spectr), np.mean(phase_spectr)

def generate_frame_with_features(frame, spectral_type = 'zorin_ageev'):
    global i
    frame_with_spectral_properties = frame.copy()
    # Reset counter
    i = 0
    frame_with_spectral_properties[['min', 'max', 'mean']] = frame_with_spectral_properties.apply(calc_spectral_properties, axis=1, spectral_type=spectral_type).tolist()
    frame_with_spectral_properties['relation'] = np.log(frame_with_spectral_properties['min']/frame_with_spectral_properties['max'])
    frame_with_spectral_properties['min'] = np.log(frame_with_spectral_properties['min'])
    frame_with_spectral_properties['max'] = np.log(frame_with_spectral_properties['max'])
    frame_with_spectral_properties['mean'] = np.log(frame_with_spectral_properties['mean'])
    return frame_with_spectral_properties

def feature_scatter_plot(sample_frame: pd.DataFrame) -> None:
    # Setup dictionary
    features = ['max', 'min', 'relation', 'mean']
    latex_labels = [r'$\max\,\sigma(\varphi)$', r'$\min\,\sigma(\varphi)$', 
                    r'$\frac{\min\,\varphi(\omega)}{\max\,\varphi(\omega)}$',
                    r'$\langle\,\sigma(\varphi)\rangle$'] 
    latex_labels_dict = dict(zip(features, latex_labels))
    
    for feature in features:
        setup_plot(r'$\alpha$', f"{latex_labels_dict[feature]} [rad]", feature)
        plt.scatter(sample_frame['alpha'], np.exp(sample_frame[feature]), alpha=0.2)
        plt.yscale('log')
        plt.savefig(f"{path_plots}/analysis/{feature}_scatter.png", dpi=800)
        plt.show()
        print(sample_frame[feature].var()/sample_frame[feature].mean())
        
def feature_hist_plot(sample_frame: pd.DataFrame, log=True) -> None:
    # Setup dictionary
    features = ['max', 'min', 'relation', 'mean']
    
    for feature in features:
        fig, axes = plt.subplots(1, 2, figsize = (9, 3.5))
        fig.suptitle(feature, fontsize=14)
        sb.histplot(np.exp(sample_frame[feature]), ax = axes[0], bins=20)
        sb.histplot(sample_frame[feature], kde=True, ax = axes[1], bins=20)
        axes[0].set_title("linear")
        axes[1].set_title("log-scaled")
        fig.tight_layout()
        plt.savefig(f"{path_plots}/analysis/dist_{feature}.png", dpi=800)
        plt.show()

def get_feature_values_by_alpha(alpha: np.array) -> dict:
    relation = lambda spectrum: np.min(spectrum) / np.max(spectrum)
    last = lambda spectrum: spectrum[-1]

    # Setup dictionaries
    features = ['max', 'min', 'relation', 'mean', 'last']
    functions = [np.max, np.min, relation, np.mean, last]
    functions_dict = dict(zip(features, functions))
    relations_dict = {feature: [] for feature in features}

    for alpha_ in alpha:
        spectrum_ = np.angle(get_spectrum_for_alpha(alpha_))
        for feature in features:
            function = functions_dict[feature]
            val = function(spectrum_)
            relations_dict[feature].append(val)
    return relations_dict

def plot_feature_values_over_alpha(N:int = 11) -> None:
    alpha_vals = np.linspace(0, 1, N)
    features = ['max', 'min', 'relation', 'mean', 'last']
    latex_labels = [r'$\max\,\varphi(\omega)$', r'$\min\,\varphi(\omega)$', 
                    r'$\frac{\min\,\sigma(\varphi)}{\max\,\varphi(\omega)}$',
                    r'$\langle\,\varphi(\omega)\rangle$', r'$\varphi(\omega)$'] 
    relations_dict = get_feature_values_by_alpha(alpha_vals)
    latex_labels_dict = dict(zip(features, latex_labels))
    
    for feature in features:
        setup_plot(r'$\alpha$', f"{latex_labels_dict[feature]} [rad]", 
                   feature if not feature == 'last' else None)
        if feature == 'last':
            plt.plot(alpha_vals, relations_dict[feature], '-o', label='last phase value')
            plt.plot(alpha_vals, relations_dict['max'], '-o', label='max phase value')
            plt.legend()
        else:
            plt.plot(alpha_vals, relations_dict[feature], '-o')
        plt.yscale('log')
        plt.savefig(f"{path_plots}/analysis/{feature}_scatter_single.png", dpi=800)
        plt.show()

def plot_spatial_distribution_ccm(measured_frame: pd.DataFrame) -> None:
    for col in measured_frame.columns:
        if col == 'spectrum_ccm':
            continue
        latex_col = latex_mapping[col]
        latex_col_with_unit = get_latex_string_with_unit(col)
        matrix = np.array(measured_frame[col])
        matrix = np.transpose(matrix.reshape(26, 30))
        setup_two_dimensional_profile(matrix, latex_col_with_unit, latex_col)
        plt.savefig(f"{path_plots}/analysis/spatial_distribution_ccm_{col}.png", bbox_inches='tight', dpi=1000)
        plt.show()

def plot_parameter_histograms_ccm(frame: pd.DataFrame) -> None:
    for i, col in enumerate(frame.columns):
        try:
            # Check if code is executed after the 
            if col == 'spectrum_ccm' or col == 'alpha':
                continue
            latex_string = latex_mapping[col]
            latex_string_with_unit = get_latex_string_with_unit(col)
            setup_histogram_plot(frame, col, latex_string, latex_string_with_unit)
            plt.savefig(f'{path_plots}/analysis/hist_field_data_{i}.png', dpi=800)
            plt.show()
        except Exception as err:
            raise ValueError("You added a column to the DataFrame that isn't expected")

def feature_scaling_hist_plot() -> None:
    fig, axes = plt.subplots(1, 2, figsize = (9, 3.5))
    fig.suptitle('Example Feature Scaling', fontsize=14)
    random_sample = np.random.random(20000)
    sb.histplot(random_sample, ax = axes[0], bins=20)
    sb.histplot(np.log(random_sample), ax = axes[1], bins=20)
    axes[0].set_title("linear")
    axes[1].set_title("log-scaled")
    fig.tight_layout()
    plt.savefig(f"{path_plots}/analysis/dist_feature_scaling.png", dpi=800)
    plt.show()