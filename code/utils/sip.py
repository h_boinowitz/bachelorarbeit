from typing import List
import utils.ml
from utils.sip_config import *
from utils.plot import *
import pandas as pd
from functools import partial
from scipy.optimize import curve_fit
import numpy as np
 

def get_random_parameters(n : int) -> pd.DataFrame:
    # Set fixed random seed
    np.random.seed(3)

    ## Bounds for free parameters
    epsilon_m = np.random.uniform(10, 40, n)
    alpha = np.random.uniform(0, 1, n)
    k = np.random.uniform(.1, .5, n)
    tau_i = np.random.uniform(2e-5, 4e-5, n)
    sigma_m = np.random.uniform(2e-4, 5e-4, n)
    sigma_i_DC = np.random.uniform(1e-7, 2e-6, n)

    # Load random parameters into a DataFrame
    frame = pd.DataFrame()
    frame['alpha'] = alpha
    frame['epsilon_m'] = epsilon_m
    frame['sigma_m'] = sigma_m
    frame['k'] = k
    frame['tau_i'] = tau_i
    frame['sigma_i_DC'] = sigma_i_DC

    return frame

def zorin_ageev_spectrum_from_parameters(row: List[float], complex: bool = True) -> np.array:
    alpha, epsilon_m, sigma_m, k, tau_i, sigma_i_DC = row
    sigma_m_tilde = sigma_m + 1j * omega * EPSILON_0 * epsilon_m
    sigma_i_tilde = sigma_i_DC + 1j * omega * EPSILON_0 * (epsilon_i_hf + (epsilon_i_DC - epsilon_i_hf)/(1 + (1j * omega * tau_i) ** c))
    sigma = ((1 - alpha) * sigma_m_tilde ** k + alpha * sigma_i_tilde ** k) ** (1/k)
    if complex:
        return sigma
    else:
        return np.hstack([np.abs(sigma), np.angle(sigma)])

def ccm_from_parameters(row: List[float], complex: bool = True) -> np.array:
    epsilon_hf, epsilon_DC, tau, c, rho_DC = row
    epsilon_r = epsilon_hf + (epsilon_DC - epsilon_hf)/(1 + (1j * omega * tau) ** c) + 1/(1j * omega * EPSILON_0 * rho_DC) 
    sigma_prime = 1j * omega * epsilon_r * EPSILON_0
    if complex:
        return sigma_prime
    else:
        return np.hstack([np.abs(sigma_prime), np.angle(sigma_prime)])

def add_zorin_ageev_spectrum_to_frame(frame: pd.DataFrame) -> pd.DataFrame:
    partialed_zorin_ageev_spectrum = partial(zorin_ageev_spectrum_from_parameters, complex = False)
    frame['spectrum_zorin_ageev'] = frame.apply(partialed_zorin_ageev_spectrum, axis = 1)
    return frame

def add_ccm_to_frame(frame: pd.DataFrame) -> pd.DataFrame:
    partialed_ccm_spectrum = partial(ccm_from_parameters, complex = False)
    frame['spectrum_ccm'] = frame.apply(partialed_ccm_spectrum, axis = 1)
    return frame

def zorin_ageev_spectrum_fitting(omega_both, alpha, epsilon_m, sigma_m, k, tau_i=2.2e-5, sigma_i_DC = 2e-7):
    N = len(omega_both)
    omega = omega_both[:N//2]
    spectrum_ = zorin_ageev_spectrum_from_parameters([alpha, epsilon_m, sigma_m, k, tau_i, sigma_i_DC], False)
    return spectrum_

def spectrum_ccm_fitting(omega_both, epsilon_hf, epsilon_DC, tau, c, rho_DC) -> np.array:
    N = len(omega_both)
    omega = omega_both[:N//2]
    spectrum_ = ccm_from_parameters([epsilon_hf, epsilon_DC, tau, c, rho_DC], False)
    return spectrum_


i = 0

def fit_from_zorin_ageev_to_ccm(row: pd.DataFrame, get_params: Optional[bool] = False) -> np.array:
    global i
    i += 1
    spectrum_zorin_ageev = np.array(row['spectrum_zorin_ageev'])
    try:
        popt, _ = curve_fit(spectrum_ccm_fitting, np.hstack([omega, omega]), spectrum_zorin_ageev, p0 = [5, 95, row['tau_i'], 0.7, 1/row['sigma_i_DC']], maxfev=5000)
    except Exception as err:
        return np.nan
    spectrum_ccm = spectrum_ccm_fitting(omega, *popt)
    if i % 1000 == 0:
        print(f'{i} fits finished.')
    if get_params:
        return spectrum_ccm, *popt
    return spectrum_ccm

def fit_from_ccm_to_zorin_ageev(row: pd.DataFrame, get_params: Optional[bool] = False) -> np.array:
    global i
    i += 1
    spectrum_ccm = np.array(row['spectrum_ccm'])
    bounds = ([0, 7, 1e-4, -1.9, 2e-5, 1e-7], [1, 22, 2e-3, 2, 3e-5, 2e-5])
    try:
        popt, _ = curve_fit(zorin_ageev_spectrum_fitting, np.hstack([omega, omega]), spectrum_ccm, bounds=bounds,
                           maxfev=5000)
    except Exception as err:
        return np.nan
    spectrum_zorin_ageev = zorin_ageev_spectrum_fitting(omega, *popt)
    if i % 1000 == 0:
        print(f'{i} fits finished.')
    if get_params:
        return spectrum_zorin_ageev, *popt
    return spectrum_zorin_ageev

@utils.ml.timer
def generate_frame_with_fitted_spectrum(frame: pd.DataFrame, samples: int = 50000, get_params: Optional[bool] = True):
    # Check if any spectral colum in DataFrame
    assert any([(col == 'spectrum_zorin_ageev' or col == 'spectrum_ccm') for col in frame.columns])

    # Check for reasonable sample size
    if samples > frame.shape[0]:
        samples = frame.shape[0]
    
    # Determine target spectral type
    if 'spectrum_zorin_ageev' not in frame.columns:
        target_spectral_type = 'zorin_ageev'
    elif 'spectrum_ccm' not in frame.columns:
        target_spectral_type = 'ccm'
    else:
        return frame

    fitting_function_mapping = {'zorin_ageev': fit_from_ccm_to_zorin_ageev, 'ccm': fit_from_zorin_ageev_to_ccm}
    global i
    i = 0
    frame_with_fitted_spectrum = frame.copy()
    frame_with_fitted_spectrum = frame_with_fitted_spectrum.sample(samples).reset_index(drop=True)
    if get_params:
        columns_to_insert = [f'spectrum_{target_spectral_type}']
        columns_to_insert.extend(parameter_names_by_model[target_spectral_type])
        frame_with_fitted_spectrum = frame_with_fitted_spectrum.join(pd.DataFrame(frame_with_fitted_spectrum.apply(fitting_function_mapping[target_spectral_type], axis=1, get_params=True).tolist(), columns=columns_to_insert))
    else:
        frame_with_fitted_spectrum[f'spectrum_{target_spectral_type}'] = frame_with_fitted_spectrum.apply(fitting_function_mapping[target_spectral_type], axis=1)

    return frame_with_fitted_spectrum


def add_spectra_to_pickled_frame(frame: pd.DataFrame) -> pd.DataFrame:
    requested_cols = ["alpha", "epsilon_m", "sigma_m", "k", "tau_i", "sigma_i_DC", "epsilon_HF", "epsilon_DC", "tau", "c", "rho_DC"]
    zorin_ageev_cols = requested_cols[:6] 
    ccm_cols = requested_cols[6:]

    assert all([col in frame.columns for col in requested_cols])

    zorin_ageev_frame = add_zorin_ageev_spectrum_to_frame(frame[zorin_ageev_cols].copy())
    ccm_frame = add_ccm_to_frame(frame[ccm_cols].copy())

    # Concatination of both partial frames
    frame_with_spectra = pd.concat([zorin_ageev_frame, ccm_frame], axis = 1)

    return frame_with_spectra