from utils.sip_config import latex_mapping, formatter_mapping, units_by_parameter
import pandas as pd

path_tables = '../latex_tables'
path_plots = '../plots'

def get_latex_string_with_unit(colum_name: str) -> str:
    # Sanity check for mappings
    assert all([key in units_by_parameter.keys() for key in latex_mapping.keys()])

    try:
        return rf"{latex_mapping[colum_name][:-1]}\,\left[{units_by_parameter[colum_name]}\right]$"
    except KeyError as err:
        # Fall back, if column not in the latex_mapping
        return colum_name

def get_formatters_function_dictionary(formatters_string_dictionary):
    function_formatters_dict = {}
    for col, format_ in formatters_string_dictionary.items():
        latex_column = get_latex_string_with_unit(col)
        function_formatters_dict[latex_column] = lambda x, format_ = format_: format_ % x 
    return function_formatters_dict

formatters = get_formatters_function_dictionary(formatter_mapping)

escape_percent = lambda x: x.replace("%", "\%")

def get_export_ready_frame(frame: pd.DataFrame) -> pd.DataFrame:
    return frame.copy().rename(get_latex_string_with_unit, axis=1).describe().rename(escape_percent, axis=0)
