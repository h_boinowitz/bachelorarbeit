import numpy as np

EPSILON_0 = 8.854e-12
epsilon_i_hf = 3.2
epsilon_i_DC = 93
tau_i = 2.2e-5
c = 1
rho_i_DC = 1e7

omega = np.logspace(2, 5, 100)

formatter_mapping = {"alpha": "%.2f", "epsilon_m": "%.2f", 
                    "sigma_m": "%.2e", "k": "%.2f", "tau_i": "%.2e", 
                    "sigma_i_DC": "%.3e",  "tau": "%.2e", 
                    "rho": "%.3f", "epsilon_HF": "%.3f",
                    "epsilon_DC": "%.3f", "c": "%.3f"}
latex_mapping = {'alpha': r'$\alpha$', 'epsilon_m' : r"$\epsilon_m$",
                 "sigma_m": r"$\sigma_m$", "k" : r"$k$", "tau_i": r"$\tau_i$",
                 "sigma_i_DC" : r"$\sigma_{i,\,\mathrm{DC}}$",
                 "rho": r'$\rho$', "epsilon_HF": r"$\epsilon_\mathrm{HF}$",
                 "epsilon_DC": r"$\epsilon_\mathrm{DC}$", "tau": r"$\tau$",
                 "c": r'$c$'}
units_by_parameter = {"alpha": "1", "epsilon_m": "1",
                    "sigma_m":r"\mathrm{\frac{S}{m}}", "k": "1", "tau_i": r"\mathrm{s}", 
                    "sigma_i_DC" : r"\frac{S}{m}", 
                    "rho": r"\Omega \mathrm{m}", "epsilon_HF": "1",
                    "epsilon_DC": "1", "tau": r"\mathrm{s}", 
                    "c": "1"
                    }
parameter_names_by_model = {'zorin_ageev' : ["alpha", "epsilon_m", "sigma_m", "k", "tau_i", "sigma_i_DC"],
'ccm': ["epsilon_HF", "epsilon_DC", "tau", "c", "rho_DC"]}