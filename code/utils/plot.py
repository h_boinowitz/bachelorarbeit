from typing import Optional
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sb

def setup_plot(xlabel, ylabel, title: str = None, plot_type: str = 'lin'):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    if title:
        plt.title(title, pad=20)

    plt.tick_params('y', right=True, direction='in', which='both')
    plt.tick_params('x', top=True, direction='in', which='both')

    if 'log' in plot_type:
        plt.xscale('log')
    if plot_type == 'log':
        plt.yscale('log')

def setup_spectral_plot(phase: bool = True) -> plt.Axes:
    # Setup plots
    fig, axs =  plt.subplots(2, 1, sharex='col', figsize=(8,10))
    ylabel_lower = axs[0].set_ylabel(r'$|\sigma|\,$[$(\Omega$m)$^{-1}$]')
    xlabel_upper = axs[1].set_xlabel('$\omega\,$[Hz]')
    ylabel_upper = axs[1].set_ylabel(r'$\varphi\,$[deg]')

    if not phase:
        ylabel_upper = axs[1].set_ylabel(r'Im $\sigma\,$[$(\Omega$m)$-1$]')
        ylabel_lower = axs[0].set_ylabel(r'Re $\sigma\,$[$(\Omega$m)$-1$]')

    axs[0].set_xscale('log')
    axs[1].set_xscale('log')
    axs[0].set_yscale('log')

    axs[0].yaxis.set_tick_params(which='both', right=True, direction='in')
    axs[0].xaxis.set_tick_params(which='both', top=True, direction='in')
    axs[0].minorticks_on()

    axs[1].yaxis.set_tick_params(which='both', right=True, direction='in')
    axs[1].xaxis.set_tick_params(which='both', top=True, direction='in')
    axs[1].minorticks_on()

    return axs

def setup_histogram_plot(frame, col, title: Optional[str] = None, xlabel: Optional[str] = None, nbins: Optional[int] = 40, mean: Optional[bool] = True) -> None:
    if title:
        plt.title(title)
    sb.histplot(frame[col], bins = nbins, alpha=.50)
    if mean:
        bins = np.histogram(np.array(frame[col]), bins=nbins)[0]
        plt.axhline(np.mean(bins), color='C1', linestyle='--')
    if xlabel:
        plt.xlabel(xlabel)
    elif title:
        plt.xlabel(title)

def setup_two_dimensional_profile(matrix, cbar_label: str, title: str = None, cmap_segements: int = None):
    # Define distrecte colormap:
    cmp = plt.cm.get_cmap('jet', cmap_segements)    
    fig = plt.figure(frameon=False)
    im = plt.imshow(matrix, cmap=cmp, extent=[0,45,-8,0])
    fig.colorbar(im, fraction=0.01, pad=0.04, label = cbar_label)
    plt.xlabel('x[m]')
    plt.ylabel('z[m]')
    if title:
        plt.title(title)