import streamlit as st
import seaborn as sb
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# Set range for omega
omega = np.logspace(2, 5, 100)

st.write(
    """
    # Interactive Zorin-Ageev and CCM Spectra
    """
)

EPSILON_0 = 8.854e-12
epsilon_i_hf = 3.2
epsilon_i_DC = 93
c = 1
rho_i_DC = 1e7


def ice_content_spectrum(omega, alpha, epsilon_m, sigma_m, k, tau_i=2.2e-5, sigma_i_DC=2e-7):
    sigma_m_tilde = sigma_m + 1j * omega * EPSILON_0 * epsilon_m
    sigma_i_tilde = sigma_i_DC + 1j * omega * EPSILON_0 * (
                epsilon_i_hf + (epsilon_i_DC - epsilon_i_hf) / (1 + (1j * omega * tau_i)))
    sigma = ((1 - alpha) * sigma_m_tilde ** k + alpha * sigma_i_tilde ** k) ** (1 / k)

    return sigma


def ice_content_spectrum_both(*args):
    spectrum_ = ice_content_spectrum(*args)
    return np.hstack([np.abs(spectrum_), np.angle(spectrum_)])


def cole_cole_phase(omega, epsilon_hf, epsilon_DC, tau, c_, rho_DC):
    epsilon_r = epsilon_hf + (epsilon_DC - epsilon_hf)/(1 + (1j * omega * tau) ** c_) + 1/(1j * omega * EPSILON_0 * rho_DC)
    sigma_prime = 1j * omega * epsilon_r * EPSILON_0
    return np.angle(sigma_prime)


def spectrum_ccm(omega, epsilon_hf, epsilon_DC, tau, c, rho_DC):
    epsilon_r = epsilon_hf + (epsilon_DC - epsilon_hf) / (1 + (1j * omega * tau) ** c) + 1 / (
                1j * omega * EPSILON_0 * rho_DC)
    sigma_prime = 1j * omega * epsilon_r * EPSILON_0

    return sigma_prime


def ccm_spectrum_both(omega_both, epsilon_hf, epsilon_DC, tau, c, rho_DC):
    N = len(omega_both)
    omega = omega_both[:N//2]
    spectrum_ = spectrum_ccm(omega, epsilon_hf, epsilon_DC, tau, c, rho_DC)
    return np.hstack([np.abs(spectrum_), np.angle(spectrum_)])



def fit_ccm(ice_spectr):
    popt, _ = curve_fit(ccm_spectrum_both, np.hstack([omega, omega]), ice_spectr,
                        maxfev=int(1e9), p0 = [5, 95, tau_i, 0.5, 1/sigma_i_DC])
    spectrum_ccm_ = spectrum_ccm(omega, *popt)

    residuals = (np.angle(spectrum_ccm_, deg=True) - np.angle(spectrum, deg=True))
    rmse_ = (np.sum(residuals**2)/(residuals.size))**0.5

    col2.metric('RMSE (phase)', f"{rmse_:.4f}")

    residuals = (np.abs(spectrum_ccm_) - np.abs(spectrum))
    rmse_test = (np.sum(residuals**2)/(residuals.size))**0.5

    col2.metric('RMSE (absolute value)', f"{rmse_test:.3e}")

    if phase == 'Absolute Value/Phase':
        return np.abs(spectrum_ccm_), np.angle(spectrum_ccm_, deg=True)
    return np.real(spectrum_ccm_), np.imag(spectrum_ccm_)



side_bar_title = st.sidebar.title('Values')
setup_title = st.sidebar.write('## Specify setup')
k = st.sidebar.slider(r'k', .1, 1.0, 0.2)
alpha = st.sidebar.slider(r'α', 0, 100, 25, format="%.2f%%")/100

matrix_title = st.sidebar.write('## Matrix parameters')
sigma_m = st.sidebar.slider(r'σ', 2e-4, 5e-4, 3e-4, 5e-5, format='%.2e')
epsilon_m = st.sidebar.slider(r'ε', 10, 40, 20)

ice_title = st.sidebar.write('## Ice parameters')
tau_i = st.sidebar.slider(r'τ', 2e-5, 4e-5, 2.2e-5, 2e-6, format='%.2e')
sigma_i_DC = st.sidebar.slider(r'σ (DC)', 1, 20, 2, step=2, format='%.2fe-7')/1e7

spectrum = ice_content_spectrum(omega, alpha, epsilon_m, sigma_m, k, tau_i, sigma_i_DC)

col1, col2 = st.columns([3, 1])

ccm = col2.checkbox('Show Fitted CCM')
phase = col2.selectbox('Choose spectral type', ['Absolute Value/Phase', 'Imaginary/Real Part'])
logarithmic = col2.checkbox('Logarithmic Phase', True)

fig, axs = plt.subplots(2, 1, sharex='col', figsize=(8, 10))
xlabel_upper = axs[1].set_xlabel('$\omega\,$[Hz]')


if logarithmic:
    axs[0].set_xscale('log')
axs[0].set_yscale('log')

axs[0].yaxis.set_tick_params(which='both', right=True, direction='in')
axs[0].xaxis.set_tick_params(which='both', top=True, direction='in')
axs[0].minorticks_on()

axs[1].yaxis.set_tick_params(which='both', right=True, direction='in')
axs[1].xaxis.set_tick_params(which='both', top=True, direction='in')
axs[1].minorticks_on()

# Put spectra
if phase == 'Absolute Value/Phase':
    ylabel_upper = axs[1].set_ylabel(r'$\varphi\,$[deg]')
    ylabel_lower = axs[0].set_ylabel(r'$|\sigma|\,$[$(\Omega$m)$^{-1}$]')
    axs[0].plot(omega, np.abs(spectrum))
    axs[1].plot(omega, np.angle(spectrum, deg=True))
else:
    ylabel_upper = axs[1].set_ylabel(r'Im $\sigma\,$[$(\Omega$m)$-1$]')
    ylabel_lower = axs[0].set_ylabel(r'Re $\sigma\,$[$(\Omega$m)$-1$]')
    axs[0].plot(omega, np.real(spectrum))
    axs[1].plot(omega, np.imag(spectrum))

if ccm:
    spectrum_for_fit = ice_content_spectrum_both(omega, alpha, epsilon_m, sigma_m, k, tau_i, sigma_i_DC)
    first, second = fit_ccm(spectrum_for_fit)
    axs[0].plot(omega, first)
    axs[1].plot(omega, second)

col1.pyplot(fig)
